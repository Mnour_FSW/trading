import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/notes_controller.dart';
import 'package:trading/widgets/notes_widget.dart';

class NoteScreen extends StatefulWidget {
  // const NoteScreen({Key? key}) : super(key: key);
  @override
  State<NoteScreen> createState() => _NoteScreenState();
}

class _NoteScreenState extends State<NoteScreen> {
  final storage = FlutterSecureStorage();
  final NotesController notesController = Get.put(NotesController());

  String? _isFree = '0';
  Future<void> isFree() async {
    final storage = FlutterSecureStorage();
    final String? isFree = await storage.read(key: "isFree");
    print(isFree);

    setState(() => _isFree = isFree!);
  }

  @override
  void initState() {
    isFree();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: context.theme.primaryColor,
          title: const Text('الملاحظات'),
          leading: IconButton(
            icon: const Icon(
              Icons.note_add,
              color: Colors.white,
            ),
            onPressed: () {
              if (_isFree == '1') {
                Get.snackbar('تنبيه', 'الرجاء تفعيل الاشتراك أولاَ');
              } else {
                Get.toNamed('addNote');
              }
            },
          ),
        ),
        body: _isFree == '0'
            ? Column(
                children: [
                  Expanded(
                    child: Obx(() {
                      if (notesController.isLoading.value) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            // physics: const NeverScrollableScrollPhysics(),
                            itemCount: notesController.notesList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return NotesWidget(
                                notesController: notesController,
                                index: index,
                                id: notesController.notesList[index].id,
                                title: notesController.notesList[index].title,
                                note: notesController.notesList[index].note,
                                date:
                                    notesController.notesList[index].createdAt,
                              );
                            });
                      }
                    }),
                  )
                ],
              )
            : Center(
                child: Container(
                  height: 350,
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width / 3,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/pr.png'),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'لإستخدام خدمة الملاحظات الرجاء تفعيل الاشتراك',
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: 200,
                        height: 45.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: context.theme.primaryColor),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Get.toNamed('myf');
                            },
                            child: const Center(
                              child: Text(
                                'تفعيل الاشتراك',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
