import 'package:flutter/material.dart';
import 'package:trading/widgets/recent_chats.dart';

import 'widgets/message_composer.dart';

class ChatScreen extends StatefulWidget {
  final User user;

  ChatScreen({required this.user});
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  _buildMessage(
    Message message,
    bool isMe,
  ) {
    final Container msg = Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: !isMe
                ? CircleAvatar(
                    backgroundColor: Colors.grey,
                    radius: 20.0,
                    backgroundImage: NetworkImage(
                        "https://previews.123rf.com/images/vectorv/vectorv1911/vectorv191100883/133109262-green-user-protection-icon-isolated-on-blue-background-secure-user-login-password-protected-personal.jpg"),
                  )
                : null,
          ),
          Column(
            crossAxisAlignment:
                isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: isMe
                    ? EdgeInsets.only(
                        right: 0,
                        top: 8.0,
                        bottom: 8.0,
                        left: 80.0,
                      )
                    : EdgeInsets.only(
                        top: 8.0,
                        bottom: 8.0,
                      ),
                padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
                width: MediaQuery.of(context).size.width * 0.6,
                decoration: BoxDecoration(
                    color: isMe ? Color(0xff72D49A) : Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      message.text,
                      style: TextStyle(
                        color: isMe ? Colors.white : Colors.blueGrey,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: isMe
                    ? const EdgeInsets.only(left: 90.0)
                    : const EdgeInsets.only(right: 10.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.access_alarm,
                      color: Colors.blueGrey,
                      size: 15,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      message.time,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 12.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: isMe
                ? CircleAvatar(
                    backgroundColor: Colors.grey,
                    radius: 20.0,
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
    if (isMe) {
      return msg;
    }
    return Row(
      children: <Widget>[
        msg,
        // IconButton(
        //   icon: message.isLiked
        //       ? Icon(Icons.favorite)
        //       : Icon(Icons.favorite_border),
        //   iconSize: 30.0,
        //   color: message.isLiked ? Colors.red : Colors.red,
        //   onPressed: () {},
        // )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xFF2B2C56),
      appBar: AppBar(
        backgroundColor: Color(0xff71c93e),
        title: Center(
          child: Text(
            widget.user.name,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            iconSize: 30.0,
            color: Colors.white,
            onPressed: () {},
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFFE8E8E8),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                  child: ListView.builder(
                    reverse: false,
                    padding: EdgeInsets.only(top: 15.0),
                    itemCount: messages.length,
                    itemBuilder: (BuildContext context, int index) {
                      final Message message = messages[index];
                      final bool isMe = message.sender.id == admin.id;
                      return _buildMessage(message, isMe);
                    },
                  ),
                ),
              ),
            ),
            MessageComposer(),
          ],
        ),
      ),
    );
  }
}
