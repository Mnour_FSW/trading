import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:trading/widgets/chat_widgets.dart';
import 'package:trading/widgets/recent_chats.dart';

import 'controllers/chats_controller.dart';

class MainChatScreen extends StatefulWidget {
  @override
  _MainChatScreenState createState() => _MainChatScreenState();
}

class _MainChatScreenState extends State<MainChatScreen> {
  String? _userTybe;
  Future<void> getName() async {
    final storage = FlutterSecureStorage();
    final String? userTybe = await storage.read(key: "type");
    print(userTybe);

    setState(() => _userTybe = userTybe!);
  }

  String? _isFree = '0';
  Future<void> isFree() async {
    final storage = FlutterSecureStorage();
    final String? isFree = await storage.read(key: "isFree");
    print(isFree);

    setState(() => _isFree = isFree!);
  }

  final ChatsController chatsController = Get.put(ChatsController());

  @override
  void initState() {
    getName();

    isFree();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.grey[200],
          appBar: AppBar(
            title: Text("المحادثات"),
            backgroundColor: Color(0xff71c93e),
            leading: Center(),
          ),
          body: _isFree == '0'
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          print("massages");
                          Get.toNamed(
                            'massages',
                          );
                        },
                        child: ChatWidget(
                          id: 1,
                          name: "المجموعة العامة",
                          text: "كل رسائل المستشار",
                          image:
                              "https://previews.123rf.com/images/vectorv/vectorv1911/vectorv191100883/133109262-green-user-protection-icon-isolated-on-blue-background-secure-user-login-password-protected-personal.jpg",
                        ),
                      ),
                        GestureDetector(
                        onTap: () {
                          print("massages");
                          Get.toNamed(
                            'massages',
                          );
                        },
                        child: ChatWidget(
                          id: 1000,
                          name: "الدعم التقني",
                          text: "",
                          image:
                              "https://png.pngtree.com/png-vector/20190214/ourlarge/pngtree-customer-support-icon-graphic-design-template-vector-png-image_384606.jpg",
                        ),
                      ),
                      Expanded(
                        child: Obx(() {
                          if (chatsController.isLoading.value) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                padding: EdgeInsets.all(0.0),
                                itemCount: chatsController.chatsList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      Get.toNamed(
                                        'news?id=${chatsController.chatsList[index].id}&name=${chatsController.chatsList[index].name}&image=${chatsController.chatsList[index].image}',
                                      );
                                    },
                                    child: ChatWidget(
                                      id: chatsController.chatsList[index].id,
                                      name:
                                          chatsController.chatsList[index].name,
                                      text: "",
                                      image:
                                          'https://cdn-icons-png.flaticon.com/512/149/149071.png',
                                    ),
                                    // NewWidget(
                                    //   id: chatsController.chatsList[index].id,
                                    //   title: newsController.newsList[index].title,
                                    //   image: newsController.newsList[index].image,
                                    //   articel:
                                    //       newsController.newsList[index].articel,
                                    //   date: newsController.newsList[index].createdAt
                                    //       .toString(),
                                    // ),
                                  );
                                  // return SoukCard(
                                  //   name: newsController
                                  //       .newsList[index]
                                  //       .name,
                                  //   code: marketController
                                  //       .marketList[index]
                                  //       .code,
                                  //   id: marketController
                                  //       .marketList[index].id,
                                  //   image:
                                  //       'http://www.trading.algorexe.com/images/${marketController.marketList[index].image}',
                                  // );
                                });
                          }
                        }),
                      ),
                      SizedBox(
                        height: 30,
                      )
                    ],
                  ),
                )
              : Center(
                  child: Container(
                    height: 350,
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.grey[200],
                          radius: 100,
                          backgroundImage: AssetImage('assets/images/pr.png'),
                        ),
                        Text(
                          'لإستخدام خدمة المحادثات الرجاء تفعيل الاشتراك',
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: 200,
                          height: 45.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: const Color(0xff71c93e)),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                Get.toNamed('myf');
                              },
                              child: const Center(
                                child: Text(
                                  'تفعيل الاشتراك',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ));
  }
}
