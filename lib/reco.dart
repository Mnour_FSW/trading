import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get_utils/src/extensions/context_extensions.dart';
import 'package:get/instance_manager.dart';
import 'package:trading/controllers/reco_controller.dart';

class RecoScreen extends StatefulWidget {
  @override
  State<RecoScreen> createState() => _RecoScreenState();
}

class _RecoScreenState extends State<RecoScreen> {
  final RecoController recoController = Get.put(RecoController());

  String? _isFree = '0';
  Future<void> isFree() async {
    final storage = FlutterSecureStorage();
    final String? isFree = await storage.read(key: "isFree");
    print(isFree);

    setState(() => _isFree = isFree!);
  }

  @override
  void initState() {
    isFree();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: dead_code
    recoController.fetchreco(Get.arguments);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Color(0xffF8F8F8),
        appBar: AppBar(
          backgroundColor: context.theme.primaryColor,
          title: const Text('الاستشارات الجديدة'),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Get.offNamed('home');
            },
          ),
        ),
        body: _isFree == '0'
            ? Column(
                children: [
                  Expanded(
                    child: Obx(() {
                      if (recoController.isLoading.value) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (recoController.isEmpties == true) {
                        return Center(
                          child: Container(
                            height: 270,
                            child: Column(
                              children: [
                                CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 100,
                                  backgroundImage:
                                      AssetImage('assets/images/e.jpeg'),
                                ),
                                Text(
                                  'لا يوجد استشارات جديدة',
                                  style: TextStyle(
                                      color: Colors.grey[800],
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        );
                      } else {
                        return ListView.builder(
                          itemCount: recoController.recokList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                                width: MediaQuery.of(context).size.width - 40,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 20),
                                padding: const EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.4),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: const Offset(0, 0),
                                    ),
                                  ],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width -
                                          80,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            '${recoController.recokList[index].createdAt}',
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          )
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    recoController.recokList[index].image !=
                                            null
                                        ? GestureDetector(
                                            child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  2,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: BoxDecoration(
                                                color: Colors.grey,
                                                image: DecorationImage(
                                                  image: CachedNetworkImageProvider(
                                                      'http://www.trading.algorexe.com/images/${recoController.recokList[index].image}'),
                                                  fit: BoxFit.cover,
                                                ),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(8.0)),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          // padding: EdgeInsets.all(10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              80,
                                          child: Text(
                                            'الاستشارة :${recoController.recokList[index].title}',
                                            style:
                                                const TextStyle(fontSize: 14),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text(
                                          'الدعوم :',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          recoController.recokList[index].up,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text(
                                          'المقاومات :',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          recoController.recokList[index].down,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.green,
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (recoController.recokList[index].notes !=
                                        null)
                                      Row(
                                        children: const [
                                          Text(
                                            'الملاحظات :',
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ],
                                      )
                                    else
                                      Container(),
                                  ],
                                ));
                          },
                        );
                      }
                    }),
                  ),
                ],
              )
            : Center(
                child: Container(
                  height: 350,
                  child: Column(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.grey[200],
                        radius: 100,
                        backgroundImage: AssetImage('assets/images/pr.png'),
                      ),
                      Text(
                        'لإستخدام خدمة الإستشارات اليومية الرجاء تفعيل الاشتراك',
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: 200,
                        height: 45.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: context.theme.primaryColor),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Get.toNamed('myf');
                            },
                            child: const Center(
                              child: Text(
                                'تفعيل الاشتراك',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
