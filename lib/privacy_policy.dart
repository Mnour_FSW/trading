import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/context_extensions.dart';

class PrivacyPolicy extends StatelessWidget {
  const PrivacyPolicy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text('سياسة الخصوصية'),
          backgroundColor: context.theme.primaryColorDark,
        ),
        body: Directionality(
          textDirection: TextDirection.rtl,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'سياسة الخصوصية',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'تم التحديث في 2022-02-27',
                      style: TextStyle(fontSize: 11, color: Colors.black54),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'يلتزم نادي خبراء السوق ("نحن" أو "خاصتنا" أو "نحن") بحماية خصوصيتك. توضح سياسة الخصوصية هذه كيف يتم جمع معلوماتك الشخصية واستخدامها والإفصاح عنها بواسطة Market Experts Club. \nتنطبق سياسة الخصوصية هذه على تطبيقنا المسمى Market Experts Club والنطاقات الفرعية المرتبطة به (يشار إليها إجمالاً باسم "خدمتنا"). من خلال الوصول إلى خدمتنا أو استخدامها ، فإنك تشير إلى أنك قد قرأت وفهمت ووافقت على جمع معلوماتك الشخصية وتخزينها واستخدامها والإفصاح عنها كما هو موضح في سياسة الخصوصية وشروط الخدمة الخاصة بنا.',
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'التعاريف والمصطلحات الرئيسية',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'للمساعدة في شرح الأشياء بأكبر قدر ممكن من الوضوح في سياسة الخصوصية هذه ، في كل مرة تتم الإشارة إلى أي من هذه الشروط ، يتم تعريفها بدقة على النحو التالي:'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 3.0),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'ملف تعريف الارتباط: كمية صغيرة من البيانات يتم إنشاؤها بواسطة موقع ويب وحفظها بواسطة متصفح الويب الخاص بك. يتم استخدامه لتحديد متصفحك ، وتقديم التحليلات ، وتذكر معلومات عنك مثل تفضيلات اللغة أو معلومات تسجيل الدخول.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'الشركة: عندما تشير هذه السياسة إلى "الشركة" أو "نحن" أو "نحن" أو "خاصتنا" ، فإنها تشير إلى نادي خبراء السوق المسؤول عن معلوماتك بموجب سياسة الخصوصية هذه.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'الدولة: حيث يوجد نادي خبراء السوق أو أصحاب / مؤسسي نادي خبراء السوق ، في هذه الحالة هي دولة الإمارات العربية المتحدة.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'العميل: يشير إلى الشركة أو المنظمة أو الشخص الذي قام بالتسجيل لاستخدام خدمة نادي خبراء السوق لإدارة العلاقات مع المستهلكين أو مستخدمي الخدمة.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'الجهاز: أي جهاز متصل بالإنترنت مثل الهاتف أو الكمبيوتر اللوحي أو الكمبيوتر أو أي جهاز آخر يمكن استخدامه لزيارة Market Experts Club واستخدام الخدمات.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'عنوان IP: يتم تخصيص رقم يعرف بعنوان بروتوكول الإنترنت (IP) لكل جهاز متصل بالإنترنت. عادة ما يتم تعيين هذه الأرقام في كتل جغرافية. يمكن استخدام عنوان IP غالبًا لتحديد الموقع الذي يتصل منه الجهاز بالإنترنت.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'الموظفون: يشير إلى هؤلاء الأفراد الذين تم توظيفهم من قبل نادي خبراء السوق أو بموجب عقد لأداء خدمة نيابة عن أحد الأطراف.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'البيانات الشخصية: أي معلومات تسمح بشكل مباشر أو غير مباشر أو فيما يتعلق بمعلومات أخرى - بما في ذلك رقم التعريف الشخصي - بتحديد هوية الشخص الطبيعي أو التعرف عليه. الخدمة: تشير إلى الخدمة المقدمة من Market Experts Club كما هو موضح في المصطلحات النسبية (إن وجدت) وعلى هذه المنصة.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'خدمة الطرف الثالث: تشير إلى المعلنين ورعاة المسابقة والشركاء الترويجيين والتسويق وغيرهم ممن يقدمون المحتوى الخاص بنا أو الذين نعتقد أن منتجاتهم أو خدماتهم قد تهمك.'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'موقع الويب: موقع نادي خبراء السوق ، والذي يمكن الوصول إليه عبر عنوان URL هذا: ___________'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'أنت: شخص أو كيان مسجل في نادي خبراء السوق لاستخدام الخدمات.'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'ما هي المعلومات التي نقوم بجمعها؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نقوم بجمع معلومات منك عندما تزور خدمتنا ، أو تقوم بالتسجيل ، أو تقديم طلب ، أو الاشتراك في النشرة الإخبارية لدينا ، أو الرد على استبيان أو ملء نموذج.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        '- الاسم / اسم المستخدم \n- أرقام الهاتف \n- عناوين البريد الإلكتروني \n- كلمة المرور'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نقوم أيضًا بجمع المعلومات من الأجهزة المحمولة للحصول على تجربة مستخدم أفضل ، على الرغم من أن هذه الميزات اختيارية تمامًا:'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        '- الكاميرا (الصور): يسمح منح إذن الكاميرا للمستخدم بتحميل أي صورة مباشرة من خدمتنا. يمكنك بأمان رفض أذونات الكاميرا لخدمتنا.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        '- معرض الصور (الصور): يتيح منح الوصول إلى معرض الصور للمستخدم تحميل أي صورة من معرض الصور الخاص به ، ويمكنك بأمان رفض الوصول إلى معرض الصور لخدمتنا.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'كيف نستخدم المعلومات التي نجمعها؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'يمكن استخدام أي من المعلومات التي نجمعها منك بإحدى الطرق التالية:'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 3.0),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'لتخصيص تجربتك (تساعدنا معلوماتك على الاستجابة بشكل أفضل لاحتياجاتك الفردية)'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'لتحسين خدماتنا (نسعى باستمرار لتحسين عروض خدماتنا بناءً على المعلومات والتعليقات التي نتلقاها منك)'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'لتحسين خدمة العملاء (تساعدنا معلوماتك على الاستجابة بشكل أكثر فعالية لطلبات خدمة العملاء واحتياجات الدعم)'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(child: Text('لمعالجة المعاملات')),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                    'لإدارة مسابقة أو ترويج أو استطلاع أو ميزة أخرى في الموقع'),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.fiber_manual_record,
                                  size: 14,
                                ),
                              ),
                              Expanded(
                                  child:
                                      Text('لإرسال رسائل بريد إلكتروني دورية')),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'متى نستخدم معلومات العملاء من أطراف ثالثة؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نتلقى بعض المعلومات من الأطراف الثالثة عندما تتصل بنا. على سبيل المثال ، عندما ترسل عنوان بريدك الإلكتروني إلينا لإظهار اهتمامك بأن تصبح عميلاً لنا ، فإننا نتلقى معلومات من طرف ثالث يوفر لنا خدمات الكشف عن الاحتيال آليًا. كما نقوم أحيانًا بجمع المعلومات التي يتم إتاحتها للجمهور على مواقع التواصل الاجتماعي. يمكنك التحكم في مقدار المعلومات التي تنشرها مواقع التواصل الاجتماعي على الإنترنت من خلال زيارة هذه المواقع وتغيير إعدادات الخصوصية الخاصة بك.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'هل نشارك المعلومات التي نجمعها مع أطراف ثالثة؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'قد نشارك المعلومات التي نجمعها ، الشخصية وغير الشخصية ، مع أطراف ثالثة مثل المعلنين ورعاة المسابقة وشركاء الترويج والتسويق وغيرهم ممن يقدمون المحتوى الخاص بنا أو الذين نعتقد أن منتجاتهم أو خدماتهم قد تهمك. يجوز لنا أيضًا مشاركتها مع الشركات التابعة لنا الحالية والمستقبلية وشركاء الأعمال ، وإذا كنا مشاركين في عملية اندماج أو بيع أصول أو إعادة تنظيم أعمال أخرى ، فقد نشارك أو ننقل معلوماتك الشخصية وغير الشخصية إلى من يخلفنا. -فائدة.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'قد نقوم بإشراك موفري خدمات الطرف الثالث الموثوق بهم لأداء الوظائف وتقديم الخدمات لنا ، مثل استضافة وصيانة خوادمنا وخدمتنا ، وتخزين وإدارة قاعدة البيانات ، وإدارة البريد الإلكتروني ، وتسويق التخزين ، ومعالجة بطاقات الائتمان ، وخدمة العملاء ، وتنفيذ الطلبات للمنتجات والخدمات التي قد تشتريها من خلال خدمتنا. من المحتمل أن نشارك معلوماتك الشخصية ، وربما بعض المعلومات غير الشخصية ، مع هذه الأطراف الثالثة لتمكينهم من أداء هذه الخدمات لنا ومن أجلك.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'يجوز لنا مشاركة أجزاء من بيانات ملف السجل لدينا ، بما في ذلك عناوين IP ، لأغراض التحليلات مع أطراف ثالثة مثل شركاء تحليلات الويب ومطوري التطبيقات والشبكات الإعلانية. إذا تمت مشاركة عنوان IP الخاص بك ، فيمكن استخدامه لتقدير الموقع العام والتقنيات الأخرى مثل سرعة الاتصال ، وما إذا كنت قد زرت الخدمة في موقع مشترك ، ونوع الجهاز المستخدم لزيارة الخدمة. قد يقومون بتجميع معلومات حول إعلاناتنا وما تراه على الخدمة ثم تقديم التدقيق والبحث وإعداد التقارير لنا وللمعلنين لدينا.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'قد نكشف أيضًا عن معلومات شخصية وغير شخصية عنك للحكومة أو مسؤولي إنفاذ القانون أو الأطراف الخاصة لأننا ، وفقًا لتقديرنا الخاص ، نعتقد أنها ضرورية أو مناسبة للرد على المطالبات ، والإجراءات القانونية (بما في ذلك مذكرات الاستدعاء) ، لحماية الحقوق والمصالح أو تلك الخاصة بطرف ثالث ، أو سلامة الجمهور أو أي شخص ، لمنع أو إيقاف أي نشاط غير قانوني أو غير أخلاقي أو عملي قانونيًا ، أو الامتثال لأوامر المحكمة والقوانين والقواعد واللوائح المعمول بها.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'أين ومتى يتم جمع المعلومات من العملاء والمستخدمين النهائيين؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'سنجمع المعلومات الشخصية التي ترسلها إلينا. قد نتلقى أيضًا معلومات شخصية عنك من جهات خارجية كما هو موضح أعلاه.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'كيف نستخدم عنوان بريدك الإلكتروني؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'بإرسال عنوان بريدك الإلكتروني على خدمتنا ، فإنك توافق على تلقي رسائل بريد إلكتروني منا. يمكنك إلغاء مشاركتك في أي من قوائم البريد الإلكتروني هذه في أي وقت عن طريق النقر فوق ارتباط إلغاء الاشتراك أو خيار إلغاء الاشتراك الآخر المضمن في البريد الإلكتروني المعني. نحن نرسل رسائل البريد الإلكتروني فقط إلى الأشخاص الذين سمحوا لنا بالاتصال بهم ، إما بشكل مباشر أو من خلال طرف ثالث. نحن لا نرسل رسائل بريد إلكتروني تجارية غير مرغوب فيها ، لأننا نكره البريد العشوائي مثلك تمامًا. من خلال إرسال عنوان بريدك الإلكتروني ، فإنك توافق أيضًا على السماح لنا باستخدام عنوان بريدك الإلكتروني لاستهداف جمهور العملاء على مواقع مثل Facebook ، حيث نعرض إعلانات مخصصة لأشخاص محددين قاموا بالاشتراك لتلقي الاتصالات منا. سيتم استخدام عناوين البريد الإلكتروني المقدمة فقط من خلال صفحة معالجة الطلب لغرض وحيد هو إرسال المعلومات والتحديثات المتعلقة بطلبك. ومع ذلك ، إذا كنت قد قدمت لنا نفس البريد الإلكتروني من خلال طريقة أخرى ، فيجوز لنا استخدامها لأي من الأغراض المذكورة في هذه السياسة. ملاحظة: إذا كنت ترغب في أي وقت في إلغاء الاشتراك من تلقي رسائل البريد الإلكتروني المستقبلية ، فإننا نقوم بتضمين إرشادات مفصلة لإلغاء الاشتراك في أسفل كل بريد إلكتروني.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'هل يمكن نقل معلوماتي إلى دول أخرى؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'تم تأسيسنا في الإمارات العربية المتحدة. قد يتم نقل المعلومات التي يتم جمعها عبر موقعنا الإلكتروني ، أو من خلال التفاعلات المباشرة معك ، أو من استخدام خدمات المساعدة الخاصة بنا من وقت لآخر إلى مكاتبنا أو موظفينا ، أو إلى جهات خارجية ، الموجودة في جميع أنحاء العالم ، ويمكن عرضها واستضافتها في أي مكان في العالم. العالم ، بما في ذلك البلدان التي قد لا يكون لديها قوانين ذات تطبيق عام تنظم استخدام ونقل هذه البيانات. إلى أقصى حد يسمح به القانون المعمول به ، باستخدام أي مما سبق ، فإنك توافق طواعية على نقل واستضافة هذه المعلومات عبر الحدود.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'هل المعلومات التي يتم جمعها من خلال خدمتنا آمنة؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نتخذ الاحتياطات اللازمة لحماية أمن معلوماتك. لدينا إجراءات مادية وإلكترونية وإدارية للمساعدة في الحماية ومنع الوصول غير المصرح به والحفاظ على أمان البيانات واستخدام معلوماتك بشكل صحيح. ومع ذلك ، لا الأشخاص ولا أنظمة الأمان مضمونة ، بما في ذلك أنظمة التشفير. بالإضافة إلى ذلك ، يمكن أن يرتكب الأشخاص جرائم متعمدة أو يرتكبون أخطاء أو يفشلون في اتباع السياسات. لذلك ، بينما نبذل جهودًا معقولة لحماية معلوماتك الشخصية ، لا يمكننا ضمان أمنها المطلق. إذا كان القانون المعمول به يفرض أي واجب غير قابل لإخلاء المسؤولية لحماية معلوماتك الشخصية ، فأنت توافق على أن سوء السلوك المتعمد سيكون هو المعايير المستخدمة لقياس امتثالنا لهذا الواجب.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'هل يمكنني تحديث أو تصحيح معلوماتي؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'تعتمد الحقوق التي تتمتع بها في طلب التحديثات أو التصحيحات على المعلومات التي نجمعها على علاقتك معنا. يجوز للموظفين تحديث أو تصحيح معلوماتهم على النحو المفصل في سياسات التوظيف الداخلية لشركتنا. يحق للعملاء طلب تقييد استخدامات معينة والإفصاح عن معلومات التعريف الشخصية على النحو التالي. يمكنك الاتصال بنا من أجل (1) تحديث أو تصحيح معلومات التعريف الشخصية الخاصة بك ، (2) تغيير تفضيلاتك فيما يتعلق بالاتصالات والمعلومات الأخرى التي تتلقاها منا ، أو (3) حذف معلومات التعريف الشخصية المحفوظة عنك على موقعنا الأنظمة (مع مراعاة الفقرة التالية) بإلغاء حسابك. لن يكون لهذه التحديثات والتصحيحات والتغييرات والحذف أي تأثير على المعلومات الأخرى التي نحتفظ بها ، أو المعلومات التي قدمناها لأطراف ثالثة وفقًا لسياسة الخصوصية هذه قبل هذا التحديث أو التصحيح أو التغيير أو الحذف. لحماية خصوصيتك وأمانك ، قد نتخذ خطوات معقولة (مثل طلب كلمة مرور فريدة) للتحقق من هويتك قبل منحك حق الوصول إلى ملفك الشخصي أو إجراء تصحيحات. أنت مسؤول عن الحفاظ على سرية كلمة المرور الفريدة ومعلومات الحساب في جميع الأوقات. يجب أن تدرك أنه ليس من الممكن تقنيًا إزالة كل سجل من المعلومات التي قدمتها إلينا من نظامنا. الحاجة إلى نسخ أنظمتنا احتياطيًا لحماية المعلومات من الفقد غير المقصود يعني أن نسخة من معلوماتك قد تكون موجودة في شكل غير قابل للمسح يصعب علينا أو يتعذر علينا تحديد موقعه. فور تلقي طلبك ، سيتم تحديث جميع المعلومات الشخصية المخزنة في قواعد البيانات التي نستخدمها بنشاط ، وغيرها من الوسائط القابلة للبحث بسهولة ، أو تصحيحها ، أو تغييرها أو حذفها ، حسب الاقتضاء ، في أقرب وقت وإلى الحد المعقول والعملي تقنيًا. إذا كنت مستخدمًا نهائيًا وترغب في تحديث أو حذف أو تلقي أي معلومات لدينا عنك ، فيمكنك القيام بذلك عن طريق الاتصال بالمنظمة التي أنت عميل لها.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'بيع الأعمال',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نحتفظ بالحق في نقل المعلومات إلى طرف ثالث في حالة البيع أو الدمج أو النقل الآخر لجميع أو جميع أصولنا أو أي من الشركات التابعة لها (على النحو المحدد هنا) ، أو ذلك الجزء منا أو أي من الشركات التابعة لها التي تتعلق بها الخدمة ، أو في حالة توقفنا عن عملنا أو تقديم التماس أو تقديم التماس ضدنا في حالة إفلاس أو إعادة تنظيم أو إجراء مشابه ، شريطة أن يوافق الطرف الثالث على الالتزام بالشروط من سياسة الخصوصية هذه.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'الشركات التابعة',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'قد نكشف عن معلومات (بما في ذلك المعلومات الشخصية) عنك إلى الشركات التابعة لنا. لأغراض سياسة الخصوصية هذه ، تعني "الشركة التابعة للشركة" أي شخص أو كيان يتحكم بشكل مباشر أو غير مباشر أو يتحكم فيه أو يخضع لسيطرة مشتركة معنا ، سواء عن طريق الملكية أو غير ذلك. سيتم التعامل مع أي معلومات تتعلق بك نقدمها إلى الشركات التابعة لنا من قبل تلك الشركات التابعة وفقًا لشروط سياسة الخصوصية هذه.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'كم من الوقت نحتفظ بمعلوماتك؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نحتفظ بمعلوماتك فقط طالما أننا بحاجة إليها لتقديم الخدمة لك وتحقيق الأغراض الموضحة في هذه السياسة. هذا هو الحال أيضًا بالنسبة لأي شخص نشارك معلوماتك معه ويقوم بتنفيذ الخدمات نيابة عنا. عندما لم نعد بحاجة إلى استخدام معلوماتك ولم تعد هناك حاجة لنا للاحتفاظ بها للامتثال لالتزاماتنا القانونية أو التنظيمية ، فسنقوم إما بإزالتها من أنظمتنا أو نزع طابعها الشخصي حتى لا نتمكن من التعرف عليك.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'كيف نحمي معلوماتك؟',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نقوم بتنفيذ مجموعة متنوعة من الإجراءات الأمنية للحفاظ على سلامة معلوماتك الشخصية عند تقديم طلب أو إدخال معلوماتك الشخصية أو إرسالها أو الوصول إليها. نحن نقدم استخدام خادم آمن. يتم إرسال جميع المعلومات الحساسة / الائتمانية المقدمة عبر تقنية Secure Socket Layer (SSL) ثم يتم تشفيرها في قاعدة بيانات موفري بوابة الدفع الخاصة بنا فقط ليتم الوصول إليها من قبل المصرح لهم بحقوق الوصول الخاصة إلى هذه الأنظمة ، وهي مطلوبة للحفاظ على سرية المعلومات. بعد إجراء معاملة ، لا يتم الاحتفاظ بمعلوماتك الخاصة (بطاقات الائتمان ، وأرقام الضمان الاجتماعي ، والبيانات المالية ، وما إلى ذلك) مطلقًا في الملف. ومع ذلك ، لا يمكننا ضمان أو ضمان الأمن المطلق لأي معلومات ترسلها إلينا أو نضمن عدم إمكانية الوصول إلى معلوماتك على الخدمة أو الكشف عنها أو تغييرها أو إتلافها عن طريق انتهاك أي من معلوماتنا المادية أو الفنية أو الإدارية. الضمانات.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'القانون الذي يحكم',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'تحكم قوانين دولة الإمارات العربية المتحدة ، باستثناء تعارضها مع قواعد القانون ، هذه الاتفاقية واستخدامك لخدمتنا. قد يخضع استخدامك لخدمتنا أيضًا لقوانين محلية أو وطنية أو وطنية أو دولية أخرى.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'موافقتك',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'باستخدام خدمتنا أو تسجيل حساب أو إجراء عملية شراء ، فإنك توافق على سياسة الخصوصية هذه.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'روابط لمواقع أخرى',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'تنطبق سياسة الخصوصية هذه على الخدمات فقط. قد تحتوي الخدمات على روابط لمواقع أخرى لا يتم تشغيلها أو التحكم فيها من قبلنا. نحن لسنا مسؤولين عن المحتوى أو الدقة أو الآراء المعبر عنها في هذه المواقع ، ولا نقوم بالتحقيق في هذه المواقع أو مراقبتها أو التحقق من دقتها أو اكتمالها. يرجى تذكر أنه عند استخدام رابط للانتقال من الخدمات إلى موقع ويب آخر ، فإن سياسة الخصوصية الخاصة بنا لم تعد سارية. يخضع تصفحك وتفاعلك على أي موقع ويب آخر ، بما في ذلك المواقع التي تحتوي على رابط على نظامنا الأساسي ، لقواعد وسياسات هذا الموقع. قد تستخدم هذه الأطراف الثالثة ملفات تعريف الارتباط الخاصة بها أو طرق أخرى لجمع معلومات عنك.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'ملفات تعريف الارتباط',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نستخدم "ملفات تعريف الارتباط" لتحديد مناطق موقعنا التي قمت بزيارتها. ملف تعريف الارتباط هو جزء صغير من البيانات المخزنة على جهاز الكمبيوتر أو الجهاز المحمول الخاص بك بواسطة متصفح الويب الخاص بك. نستخدم ملفات تعريف الارتباط لتخصيص المحتوى الذي تراه على موقعنا. يمكن ضبط معظم متصفحات الويب لتعطيل استخدام ملفات تعريف الارتباط. ومع ذلك ، إذا قمت بتعطيل ملفات تعريف الارتباط ، فقد لا تتمكن من الوصول إلى الوظائف الموجودة على موقعنا بشكل صحيح أو على الإطلاق. لا نضع أبدًا معلومات تعريف شخصية في ملفات تعريف الارتباط.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'بيانات الدفع',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'فيما يتعلق بأي بطاقة ائتمان أو تفاصيل معالجة الدفع الأخرى التي قدمتها لنا ، فإننا نلتزم بتخزين هذه المعلومات السرية بأكثر الطرق أمانًا قدر الإمكان.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'خصوصية الأطفال',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'نحن لا نتعامل مع أي شخص يقل عمره عن 13 عامًا. نحن لا نجمع عن قصد معلومات تعريف شخصية من أي شخص يقل عمره عن 13 عامًا. إذا كنت أحد الوالدين أو الوصي وتدرك أن طفلك قد زودنا ببيانات شخصية ، فيرجى الاتصال نحن. إذا علمنا أننا قد جمعنا بيانات شخصية من أي شخص يقل عمره عن 13 عامًا دون التحقق من موافقة الوالدين ، فإننا نتخذ خطوات لإزالة هذه المعلومات من خوادمنا.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'التغييرات على سياسة الخصوصية الخاصة بنا',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'إذا قررنا تغيير سياسة الخصوصية الخاصة بنا ، فسنقوم بنشر هذه التغييرات على هذه الصفحة و / أو تحديث تاريخ تعديل سياسة الخصوصية أدناه.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'خدمات الطرف الثالث',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'يجوز لنا عرض محتوى جهة خارجية أو تضمينه أو توفيره (بما في ذلك البيانات والمعلومات والتطبيقات وخدمات المنتجات الأخرى) أو توفير روابط لمواقع أو خدمات الطرف الثالث ("خدمات الأطراف الثالثة"). \nأنت تقر وتوافق على أننا لسنا مسؤولين عن أي خدمات لأطراف أخرى ، بما في ذلك دقتها واكتمالها وتوقيتها وصلاحيتها وامتثالها لحقوق الطبع والنشر والشرعية واللياقة والجودة أو أي جانب آخر من جوانبها. نحن لا نتحمل ولن نتحمل أي التزام أو مسؤولية تجاهك أو تجاه أي شخص أو كيان آخر عن أي خدمات لأطراف أخرى. \nيتم توفير خدمات الطرف الثالث والروابط الخاصة بها فقط من أجل التسهيل عليك ويمكنك الوصول إليها واستخدامها بالكامل على مسؤوليتك الخاصة وتخضع لشروط وأحكام هذه الأطراف الثالثة.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'تقنيات التتبع',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'التخزين المحلي\n يُعرف التخزين المحلي أحيانًا باسم تخزين DOM ، ويوفر لتطبيقات الويب طرقًا وبروتوكولات لتخزين البيانات من جانب العميل. يدعم التخزين على الويب تخزين البيانات المستمر ، على غرار ملفات تعريف الارتباط ولكن بسعة محسّنة بشكل كبير ولا توجد معلومات مخزنة في عنوان طلب HTTP.'),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'اتصل بنا',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        'لا تتردد في الاتصال بنا إذا كان لديك أي أسئلة. عبر البريد الإلكتروني:'),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'trading@algorexe.com',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Colors.black),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(''),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
