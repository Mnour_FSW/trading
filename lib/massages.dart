import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

import 'api/message_services.dart';

class MassageScreen extends StatefulWidget {
  @override
  _MassageScreen createState() => _MassageScreen();
}

// late final ViewController viewController =
//     Get.put(ViewController(Get.parameters['id']));

class _MassageScreen extends State<MassageScreen>
    with SingleTickerProviderStateMixin {
  late Query _ref;
  late String message;
  late TextEditingController messageController;
  late ScrollController scrollcontroller;
  String chatId = Get.parameters['id'].toString();
  String? _userId;
  Future<void> getId() async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    print(userId);

    setState(() => _userId = userId!);
  }

  @override
  void initState() {
    // TODO: implement initState
    messageController = TextEditingController();
    // scrollcontroller = ScrollController();
    // double _position = 1.0;
    // scrollcontroller.animateTo(_position,
    //     duration: Duration(milliseconds: 100), curve: Curves.easeOut);

    getId();
    super.initState();
    _ref = FirebaseDatabase.instance
        .reference()
        .child(Get.parameters['id'].toString());
    // .orderByChild('created_at');
  }

  @override
  Widget build(BuildContext context) {
    bool isMe = true;
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff71c93e),
          title: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey,
                  radius: 25.0,
                  backgroundImage: AssetImage('assets/images/user.png'),
                ),
              ),
              Text(Get.parameters['name'].toString()),
            ],
          ),
        ),
        body: Center(
          child: Column(
            // verticalDirection: VerticalDirection.down,
            // crossAxisAlignment: CrossAxisAlignment.end,
            // mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                child: FirebaseAnimatedList(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  sort: (a, b) => b.key!.compareTo(a.key!),
                  // sort: (a, b) => b.key!.compareTo(a.key!),

                  reverse: true,
                  query: _ref,
                  // controller: scrollcontroller,
                  itemBuilder: (BuildContext context, DataSnapshot snapshot,
                      Animation<double> animation, int index) {
                    if (snapshot.value["user_id"] == _userId.toString()) {
                      isMe = true;
                      // print('${snapshot.value["user_id"]}' +
                      //     _userId.toString());
                    } else {
                      isMe = false;
                      // print('${snapshot.value["user_id"]}' +
                      //     _userId.toString());
                    }
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: isMe
                              ? CircleAvatar(
                                  backgroundColor: Colors.grey,
                                  radius: 20.0,
                                  backgroundImage: NetworkImage(
                                      "https://previews.123rf.com/images/vectorv/vectorv1911/vectorv191100883/133109262-green-user-protection-icon-isolated-on-blue-background-secure-user-login-password-protected-personal.jpg"),
                                )
                              : null,
                        ),
                        Column(
                          crossAxisAlignment: isMe
                              ? CrossAxisAlignment.start
                              : CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              margin: isMe
                                  ? EdgeInsets.only(
                                      right: 0,
                                      top: 8.0,
                                      bottom: 8.0,
                                      left: 75.0,
                                    )
                                  : EdgeInsets.only(
                                      top: 8.0,
                                      bottom: 8.0,
                                      right:75.0,
                                    ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 25.0, vertical: 15.0),
                              width: MediaQuery.of(context).size.width * 0.6,
                              decoration: BoxDecoration(
                                  color:
                                      isMe ? Color(0xff71c93e) : Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  isMe
                                      ? Text(
                                          'Me',
                                          style: TextStyle(fontSize: 0),
                                        )
                                      : Text(snapshot.value['user_name']),
                                  Text(
                                    '${snapshot.value["massage"]}',
                                    style: TextStyle(
                                      color:
                                          isMe ? Colors.white : Colors.blueGrey,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: isMe
                                  ? const EdgeInsets.only(left: 90.0)
                                  : const EdgeInsets.only(right: 10.0),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.access_alarm,
                                    color: Colors.blueGrey,
                                    size: 15,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    '${snapshot.value["created_at"]}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: !isMe
                              ? Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.grey,
                                      radius: 20.0,
                                      child: Icon(
                                        Icons.person,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                                    ),
                                  ],
                                )
                              : null,
                        ),
                      ],
                    );
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                height: 70.0,
                color: Colors.white,
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: messageController,
                          textCapitalization: TextCapitalization.sentences,
                          onChanged: (value) {
                            message = value;
                          },
                          decoration: InputDecoration.collapsed(
                            hintText: 'ارسل رسالة',
                          ),
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.send),
                        iconSize: 25.0,
                        color: Color(0xff71c93e),
                        onPressed: () {
                          MessageService.send_message(message, chatId);
                          setState(() {
                            messageController.clear();
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
