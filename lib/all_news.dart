import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/news_controller.dart';
import 'package:trading/widgets/news_widgets.dart';

class AllNews extends StatelessWidget {
  const AllNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NewsController newsController = Get.put(NewsController());
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          backgroundColor: Colors.grey[200],
          appBar: AppBar(
            backgroundColor: context.theme.primaryColor,
            title: const Text('الدروس التعليمية'),
          ),
          body: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Expanded(
                  child: Obx(() {
                    if (newsController.isLoading.value) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return ListView.builder(
                          padding: const EdgeInsets.all(0.0),
                          itemCount: newsController.newsList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                Get.toNamed(
                                  'news?id=${newsController.newsList[index].id}&title=${newsController.newsList[index].title}&image=${newsController.newsList[index].image}&articel=${newsController.newsList[index].articel}&date=${newsController.newsList[index].createdAt}',
                                );
                              },
                              child: NewWidget(
                                id: newsController.newsList[index].id,
                                title: newsController.newsList[index].title,
                                image: newsController.newsList[index].image,
                                articel: newsController.newsList[index].articel,
                                date: newsController.newsList[index].createdAt
                                    .toString(),
                              ),
                            );
                          });
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }
}
