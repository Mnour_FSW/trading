import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/all_stock_controller.dart';
import 'package:trading/controllers/stock_controller.dart';
import 'package:trading/widgets/all_souk_widgets.dart';

import 'controllers/favorite_controller.dart';
import 'widgets/souk_widgets.dart';

class FavoriteScreen extends StatelessWidget {
  final FavoriteController favoriteController = Get.put(FavoriteController());

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          // toolbarHeight: 80,
          backgroundColor: context.theme.primaryColor,
          elevation: 0,
          title: Text(
            "المفضلة",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          centerTitle: true,
          leadingWidth: 80,
          leading: GestureDetector(
            onTap: () {
              Get.offNamed('home');
            },
            child: Icon(
              Icons.arrow_back_ios,
              size: 25,
              color: Colors.white,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: Obx(() {
                if (favoriteController.isLoading.value) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return ListView.builder(
                      padding: EdgeInsets.all(0),
                      itemCount: favoriteController.favoriteList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            favoriteController.changeviews(index,
                                favoriteController.favoriteList[index].id);

                            Get.toNamed(
                              'sahim?id=${favoriteController.favoriteList[index].id}&code=${favoriteController.favoriteList[index].code}&name=${favoriteController.favoriteList[index].name}&image=${'http://www.trading.algorexe.com/images/${favoriteController.favoriteList[index].image}'}',
                            );
                          },
                          child: SoukWidgetsCard(
                            isfavorite: 1,
                            favorite:
                                favoriteController.favoriteList[index].favorite,
                            id: favoriteController.favoriteList[index].id,
                            views: favoriteController.favoriteList[index].views,
                            code: favoriteController.favoriteList[index].code,
                            name: favoriteController.favoriteList[index].name,
                            image:
                                'http://www.trading.algorexe.com/images/${favoriteController.favoriteList[index].image}',
                          ),
                        );
                      });
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}

@override
Widget build(BuildContext context) {
  // TODO: implement build
  throw UnimplementedError();
}
