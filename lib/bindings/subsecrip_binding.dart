import 'package:get/get.dart';
import 'package:trading/controllers/subsecription_controller.dart';

class SubsecripBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SubsecriptionController());
  }
}
