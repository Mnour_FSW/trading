import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/api/notes_services.dart';
import 'package:trading/controllers/notes_controller.dart';

class NotesWidget extends StatelessWidget {

  final int id;
  final int index;
  final String? title;
  final String? note;
  final DateTime date;
  final NotesController? notesController;


  const NotesWidget({
    required this.id,
    required this.index,
    required this.title,
    required this.note,
    required this.date,
    required this.notesController,
    Key? key,
  }) : super(key: key);

  

   

  // final NotesController notesController = Get.put(NotesController());

  @override
  Widget build(BuildContext context) {

   

    return Container(
      margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.amber[200],
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            date.toString(),
            style: const TextStyle(fontSize: 11),
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  title!,
                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                height: 40,
                child: IconButton(
                  icon: const Icon(
                    Icons.delete,
                    size: 20,
                    color: Color(0xffE8504D),
                  ),
                  onPressed: () {
                  
                 
                   showDialog<String>(
        context: context,
        builder: (BuildContext context) => Directionality(
          textDirection: TextDirection.rtl,
          child: AlertDialog(
            title: const Text('حذف'),
            content: const Text('هل تريد حذف هذه الملاحظة'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Cancel'),
                child: const Text('الغاء'),
              ),
              TextButton(
                onPressed: () {
                   //NotesService.removenotes(id: id);

                    notesController!.deleteNote(index,id);
                  Navigator.pop(context, 'OK');
            
                    
                },
                child: const Text('حذف'),
              ),
            ],
          ),
        ),
      );
                    // NotesService.removenotes(id: id);
                    // print('delete');
                  },
                ),
              ),
            ],
          ),
          Text(note!)
        ],
      ),
    );
  }
}
