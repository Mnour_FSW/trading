import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatWidget extends StatelessWidget {
  final int id;
  final String name;
  final String text;
  final String image;
  // final String date;
  const ChatWidget({
    Key? key,
    required this.id,
    required this.name,
    required this.text,
    required this.image,
    // required this.date,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('hello');
        Get.toNamed(
          'massages?id=${id}&name=${name}',
        );
      },
      child: Column(
        children: [
          SizedBox(
            height: 5,
          ),
          Container(
            margin: EdgeInsets.only(
              top: 7.0,
              bottom: 7.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey,
                      radius: 25.0,
                      backgroundImage: NetworkImage(
                        image,
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          name,
                          style: TextStyle(
                            color: Colors.grey[800],
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5.0),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Text(
                            text,
                            style: TextStyle(
                              color: Colors.grey[500],
                              fontSize: 12.0,
                              fontWeight: FontWeight.w600,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Text(
                      "5:00",
                      style: TextStyle(
                        color: Colors.grey[500],
                        fontSize: 11.0,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    // view
                    //     ? Container(
                    //         width: 40.0,
                    //         height: 20.0,
                    //         decoration: BoxDecoration(
                    //           color: Colors.red,
                    //           borderRadius: BorderRadius.circular(30.0),
                    //         ),
                    //         alignment: Alignment.center,
                    //         child: Text(
                    //           'NEW',
                    //           style: TextStyle(
                    //             color: Colors.white,
                    //             fontSize: 12.0,
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //       )
                    //     : Text(''),
                  ],
                ),
              ],
            ),
          ),
          new Divider(
            color: Colors.grey[400],
          ),
        ],
      ),
    );
  }
}
