import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/favorite_controller.dart';
import 'package:trading/controllers/stock_controller.dart';

class SoukWidgetsCard extends StatelessWidget {
  final int id;

  final String name;

  final String code;
  final String image;
  SoukWidgetsCard({
    Key? key,
    required this.id,
    required this.name,
    required this.code,
    required this.image,
  }) : super(key: key);

//   @override
//   State<SoukWidgetsCard> createState() => _SoukWidgetsCardState();
// }

// class _SoukWidgetsCardState extends State<SoukWidgetsCard> {
  late final FavoriteController favoriteController =
      Get.put(FavoriteController());
  final StockController stockController = Get.put(StockController());
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only( left: 10, right: 10),
      height: 91,
      padding: const EdgeInsets.all(9),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        border: Border.all(color: Colors.grey, width: 0.1),
        color: Colors.white,
      ),
      child: Row(
        children: [
          GestureDetector(
            child: Container(
              width: 70,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: CachedNetworkImageProvider(image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 20,
                width: MediaQuery.of(context).size.width - 115,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        code,
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[800]),
                      ),
                    ),
                  ],
                ),
              ),
              Text(
                name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[500]),
              ),
              const SizedBox(
                height: 2,
              ),
            ],
          )
        ],
      ),
    );
  }
}
