import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:trading/api/api_services.dart';

class AllSoukWidgetsCard extends StatefulWidget {
  final int id;
  final String name;
  final int views;
  final String code;
  final String marketcode;
  final String image;

  late int favorite;
  AllSoukWidgetsCard({
    Key? key,
    required this.id,
    required this.name,
    required this.favorite,
    required this.views,
    required this.code,
    required this.marketcode,
    required this.image,
  }) : super(key: key);

  @override
  State<AllSoukWidgetsCard> createState() => _SoukWidgetsCardState();
}

class _SoukWidgetsCardState extends State<AllSoukWidgetsCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
      // padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: widget.marketcode == "DFM"
            ? Colors.orange.withOpacity(0.7)
            : Colors.blue.withOpacity(0.7),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 1,
            blurRadius: 2,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: Text(
              widget.marketcode,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: 120,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: Offset(0, 0),
                ),
              ],
            ),
            child: Row(
              children: [
                GestureDetector(
                  // onTap: () {
                  //   Get.toNamed(
                  //     'sahim?id=${id}&code=${code}&name=${name}&image=${image}',
                  //   );
                  // views = 0;
                  //here
                  // },
                  child: Container(
                    width: 80,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(widget.image),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(10),
                      // border: Border.all(color: Colors.grey),
                      // border:
                      //     Border.all(color: Colors.blueAccent),
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 145,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              widget.code,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800]),
                            ),
                          ),
                          Icon(
                            Icons.star,
                            color: widget.favorite == 1
                                ? Colors.amber
                                : Colors.transparent,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          widget.views != 0
                              ? Stack(children: [
                                  Container(
                                    height: 30,
                                    width: 30,
                                    child: Icon(
                                      Icons.notifications,
                                      color: Colors.green[300],
                                      size: 30,
                                    ),
                                  ),
                                  Positioned(
                                    child: Container(
                                      height: 30,
                                      width: 30,
                                      child: Center(
                                        child: Text(
                                          '${widget.views}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  )
                                ])
                              : Center(),
                        ],
                      ),
                    ),
                    Text(
                      widget.name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[500]),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            if (widget.favorite == 0) {
                              ApiService.addtofavorite(id: widget.id);
                              setState(() {
                                widget.favorite = 1;
                              });
                            } else if (widget.favorite == 1) {
                              ApiService.removefavorite(id: widget.id);
                              setState(() {
                                widget.favorite = 0;
                              });
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 2),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(color: Colors.green),
                              color: Colors.white,
                            ),
                            child: Text(
                              'اضافة إلى المفضلة',
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
