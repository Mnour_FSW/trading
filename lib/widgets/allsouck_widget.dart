import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/all_stock_controller.dart';
import 'package:trading/controllers/favorite_controller.dart';
import 'package:trading/controllers/stock_controller.dart';

class AllSoukWidgetsCard extends StatelessWidget {
  final int id;

  final String name;
  final int isfavorite;
  final int views;
  final String code;
  final String image;
  late int favorite;
  AllSoukWidgetsCard({
    Key? key,
    required this.id,
    required this.name,
    required this.favorite,
    required this.isfavorite,
    required this.views,
    required this.code,
    required this.image,
  }) : super(key: key);

//   @override
//   State<SoukWidgetsCard> createState() => _SoukWidgetsCardState();
// }

// class _SoukWidgetsCardState extends State<SoukWidgetsCard> {
  late final FavoriteController favoriteController =
      Get.put(FavoriteController());
  final AllStockController stockController = Get.put(AllStockController());
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only( left: 10, right: 10),
      height: 91,
      padding: EdgeInsets.all(9),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        border: Border.all(color: Colors.grey, width: 0.1),
        color: Colors.white,
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey.withOpacity(0.4),
        //     spreadRadius: 1,
        //     blurRadius: 2,
        //     offset: Offset(0, 0),
        //   ),
        // ],
      ),
      child: Row(
        children: [
          GestureDetector(
            child: Container(
              width: 80,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: CachedNetworkImageProvider(image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 20,
                width: MediaQuery.of(context).size.width - 115,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        code,
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[800]),
                      ),
                    ),
                    Icon(
                      Icons.star,
                      color: favorite == 1 ? Colors.amber : Colors.transparent,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    views != 0
                        ? Stack(children: [
                            Container(
                              height: 30,
                              width: 30,
                              child: Icon(
                                Icons.notifications,
                                color: Colors.green[300],
                              ),
                            ),
                            Positioned(
                              child: Container(
                                height: 20,
                                width: 30,
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 3),
                                    child: Text(
                                      '${views}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 11,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ])
                        : const Center(),
                  ],
                ),
              ),
              Text(
                name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[500]),
              ),
              const SizedBox(
                height: 2,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (favorite == 0) {
                        stockController.addtofavorite(code, id);
                      } else if (favorite == 1) {
                        if (isfavorite == 0) {
                          stockController.removefromfavorite(code, id);
                        } else {
                          favoriteController.removefromfavorite(code, id);
                        }
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 1),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                            color: favorite == 0 ? Colors.green : Colors.red),
                        color: Colors.white,
                      ),
                      child: favorite == 0
                          ? const Text(
                              'اضافة إلى المفضلة',
                              style: TextStyle(fontSize: 10),
                            )
                          : const Text(
                              'ازالة من المفضلة',
                              style: TextStyle(fontSize: 10),
                            ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 10),
                  //   decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.circular(15),
                  //     border: Border.all(color: Colors.green),
                  //     color: Colors.white,
                  //   ),
                  //   child: Text(
                  //     'كل الاستشارات',
                  //     style: TextStyle(fontSize: 12),
                  //   ),
                  // ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
