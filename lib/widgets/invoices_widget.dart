import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class InvoiceWidget extends StatefulWidget {
  const InvoiceWidget({
    Key? key,
    required this.invocesId,
    required this.status,
    required this.amount,
    required this.createdAt,
  }) : super(key: key);

  final int? invocesId;
  final String? status;
  final String? amount;
  final DateTime? createdAt;

  @override
  State<InvoiceWidget> createState() => _InvoiceWidgetState();
}

class _InvoiceWidgetState extends State<InvoiceWidget> {
  String? _userName;
  Future<void> getName() async {
    final storage = FlutterSecureStorage();
    final String? userName = await storage.read(key: "name");
    setState(() => _userName = userName!);
  }

  @override
  // ignore: must_call_super
  void initState() {
    getName();
  }

  @override
  Widget build(BuildContext context) {
    // var one = int.parse(widget.amount.toString());

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            '${widget.createdAt}',
            style: const TextStyle(
              fontSize: 10,
              color: Colors.grey,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  '$_userName',
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(widget.amount.toString(),
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ))
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Expanded(
                  child: Text(
                "رقم الإيصال : " + widget.invocesId.toString(),
                style: const TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              )),
              Text(
                widget.status.toString(),
                style: const TextStyle(
                  color: Color(0xff3C8B62),
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 30.0,
            child: Opacity(
              opacity: 0.5,
              child: Divider(
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
