import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get_utils/src/extensions/context_extensions.dart';
import 'package:get/instance_manager.dart';
import 'package:trading/controllers/stock_controller.dart';
import 'package:trading/sahim.dart';
import 'dart:math' as math;

import 'package:trading/widgets/souk_widgets.dart';

class SoukScreen extends StatelessWidget {
  final StockController stockController = Get.put(StockController());

  @override
  Widget build(BuildContext context) {
    stockController.fetchstock(Get.arguments);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: NestedScrollView(
          physics: const BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                // actions: <Widget>[
                //   IconButton(
                //       icon: Transform(
                //         alignment: Alignment.center,
                //         transform: Matrix4.rotationY(math.pi),
                //         child: Icon(Icons.reply),
                //       ),
                //       onPressed: () {}),
                //   IconButton(icon: Icon(Icons.more_horiz), onPressed: () {}),
                // ],
                leading: IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      Get.back();
                    }),
                backgroundColor: context.theme.primaryColor,
                expandedHeight: 160.0,

                snap: false,
                pinned: true,
                titleSpacing: 2,
                stretch: true,
                floating: false,
                // **Is it intended ?** flexibleSpace.title overlaps with tabs title.
                flexibleSpace: FlexibleSpaceBar(
                  title: Stack(children: []),
                  stretchModes: [
                    StretchMode.zoomBackground,
                    StretchMode.blurBackground,
                    StretchMode.fadeTitle
                  ],
                  background: Stack(children: [
                    Container(
                      height: 100,
                      color: Colors.white,
                      width: double.infinity,
                    ),
                    Container(
                      height: 150,
                      width: double.infinity,
                      decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                            colors: [
                              context.theme.primaryColorDark,
                              context.theme.primaryColor,
                              // const Color(0xFFffffff).withOpacity(0.0),
                              // const Color(0xFF1D1D1D).withOpacity(0.5),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                    ),
                    Positioned(
                      top: 115,
                      left: 25,
                      right: 25,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 50.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          "https://intlbm.com/wp-content/uploads/2021/08/DFM-logo.png"),
                                      fit: BoxFit.cover,
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.3),
                                        spreadRadius: 2,
                                        blurRadius: 5,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  //color: Colors.red,
                                  //width: MediaQuery.of(context).size.width / 2.5,
                                  height: 70,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 5,
                                      left: 8.0,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "DFM سوق دبي المالي",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 22,
                                          ),
                                        ),
                                        Text(
                                          "@DFM",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ];
          },
          body: Expanded(
            child: Obx(() {
              if (stockController.isLoading.value) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    itemCount: stockController.stockList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          stockController.changeviews(
                              index, stockController.stockList[index].id);

                          Get.toNamed(
                            'sahim?id=${stockController.stockList[index].id}&code=${stockController.stockList[index].code}&name=${stockController.stockList[index].name}&image=${'http://www.trading.algorexe.com/images/${stockController.stockList[index].image}'}',
                          );
                        },
                        child: SoukWidgetsCard(
                          isfavorite: 0,
                          favorite: stockController.stockList[index].favorite,
                          id: stockController.stockList[index].id,
                          views: stockController.stockList[index].views,
                          code: stockController.stockList[index].code,
                          name: stockController.stockList[index].name,
                          image:
                              'http://www.trading.algorexe.com/images/${stockController.stockList[index].image}',
                        ),
                      );
                    });
              }
            }),
          ),
        ),
      ),
    );
  }
}
