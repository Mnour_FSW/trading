import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class SahimImage extends StatefulWidget {
  SahimImage({Key? key}) : super(key: key);

  String image = Get.parameters['image'].toString();
  @override
  _SahimImageState createState() => _SahimImageState();
}

class _SahimImageState extends State<SahimImage> {
  @override
  void initState() {
    super.initState();
    widget.image = Get.parameters['image'].toString();
    print(widget.image);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InteractiveViewer(
        clipBehavior: Clip.none,
        child: Container(
          height: MediaQuery.of(context).size.width / 1.5,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.grey,
            image: DecorationImage(
              image: CachedNetworkImageProvider(
                  'http://www.trading.algorexe.com/images/${widget.image}'),
              fit: BoxFit.cover,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
      ),
    );
  }
}
