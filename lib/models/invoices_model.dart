// To parse this JSON data, do
//
//     final invoices = invoicesFromJson(jsonString);

import 'dart:convert';

List<Invoices> invoicesFromJson(String str) =>
    List<Invoices>.from(json.decode(str).map((x) => Invoices.fromJson(x)));

String invoicesToJson(List<Invoices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Invoices {
  Invoices({
    required this.id,
    required this.userId,
    required this.amount,
    required this.mfInvoiceStatus,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int userId;
  String mfInvoiceStatus;
  String amount;
  String status;
  DateTime createdAt;
  DateTime updatedAt;

  factory Invoices.fromJson(Map<String, dynamic> json) => Invoices(
        id: json["id"],
        userId: json["user_id"],
        mfInvoiceStatus: json["mf_invoice_status"],
        amount: json["amount"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "mf_invoice_status": mfInvoiceStatus,
        "amount": amount,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
