// To parse this JSON data, do
//
//     final stock = stockFromJson(jsonString);

import 'dart:convert';

List<Stock> stockFromJson(String str) =>
    List<Stock>.from(json.decode(str).map((x) => Stock.fromJson(x)));

String stockToJson(List<Stock> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Stock {
  Stock({
    required this.id,
    required this.uuid,
    required this.marketId,
    required this.name,
    required this.code,
    required this.marketcode,
    required this.image,
    required this.views,
    required this.favorite,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  dynamic uuid;
  int marketId;
  String name;
  String code;
  int views;
  int favorite;
  String image;
  String? marketcode;
  DateTime createdAt;
  DateTime updatedAt;

  factory Stock.fromJson(Map<String, dynamic> json) => Stock(
        id: json["id"],
        uuid: json["uuid"],
        marketId: json["market_id"],
        name: json["name"],
        code: json["code"],
        image: json["image"],
        views: json["views"],
        marketcode: json["marketcode"],
        favorite: json["favorite"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "market_id": marketId,
        "name": name,
        "code": code,
        "image": image,
        "favorite": favorite,
        "marketcode": marketcode,
        "views": views,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
