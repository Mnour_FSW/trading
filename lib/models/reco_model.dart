// To parse this JSON data, do
//
//     final reco = recoFromJson(jsonString);

import 'dart:convert';

List<Reco> recoFromJson(String str) =>
    List<Reco>.from(json.decode(str).map((x) => Reco.fromJson(x)));

String recoToJson(List<Reco> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Reco {
  Reco({
    required this.id,
    required this.stockId,
    required this.title,
    required this.uuid,
    required this.up,
    required this.up1,
    required this.up2,
    required this.down,
    required this.down1,
    required this.down2,
    required this.notes,
    required this.image,
    required this.image1,
    required this.image2,
    required this.free,
    required this.createdAt,
    required this.updatedAt,
    required this.stockCode,
    required this.stockImage,
  });

  int id;
  int stockId;
  String title;
  dynamic uuid;
  String up;
  String? up1;
  String? up2;
  String down;
  String? down1;
  String? down2;
  String? notes;
  String? image;
  String? image1;
  String? image2;
  int free;
  DateTime createdAt;
  DateTime updatedAt;
  String stockCode;
  String stockImage;

  factory Reco.fromJson(Map<String, dynamic> json) => Reco(
        id: json["id"],
        stockId: json["stock_id"],
        title: json["title"],
        uuid: json["uuid"],
        up: json["up"],
        up1: json["up1"],
        up2: json["up2"],
        down: json["down"],
        down1: json["down1"],
        down2: json["down2"],
        notes: json["notes"],
        image: json["image"],
        image1: json["image1"],
        image2: json["image2"],
        free: json["free"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        stockCode: json["stock_code"],
        stockImage: json["stock_image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "stock_id": stockId,
        "title": title,
        "uuid": uuid,
        "up": up,
        "up1": up1,
        "up2": up2,
        "down": down,
        "down1": down1,
        "down2": down2,
        "notes": notes,
        "image": image,
        "image1": image1,
        "image2": image2,
        "free": free,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "stock_code": stockCode,
        "stock_image": stockImage,
      };
}
