import 'dart:convert';

List<Courses> coursesFromJson(String str) =>
    List<Courses>.from(json.decode(str).map((x) => Courses.fromJson(x)));

String coursesToJson(List<Courses> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Courses {
  Courses({
    required this.id,
    required this.title,
    required this.link,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String title;
  String link;
  DateTime createdAt;
  DateTime updatedAt;

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
        id: json["id"],
        title: json["title"],
        link: json["link"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "link": link,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
