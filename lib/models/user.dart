import 'dart:convert';

UserModel userFromJson(String str) => UserModel.toObject(json.decode(str));

class UserModel {
  User user;

  UserModel({required this.user});

  factory UserModel.toObject(Map<String, dynamic> json) => UserModel(
        user: User.toObject(json['user']),
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
      };
}

class User {
  int id;
  String name;
  String nick_name;
  String email;
  String phone;
  int? phone_verified;
  int isFree;
  String ftoken;
  String stream_token;
  String type;
  String image;
  int active;

  User(
      {required this.id,
      required this.ftoken,
      required this.stream_token,
      required this.name,
      required this.nick_name,
      required this.phone_verified,
      required this.isFree,
      required this.email,
      required this.image,
      required this.phone,
      required this.active,
      required this.type});

  factory User.toObject(Map<String, dynamic> json) => User(
      id: json['id'],
      name: json['name'],
      nick_name: json['nick_name'],
      email: json['email'],
      image: json['image'],
      ftoken: json['remember_token']!,
      stream_token: json['stream_token']!,
      phone: json['phone'],
      active: json['active'],
      phone_verified: json['phone_verified'],
      isFree: json['isFree'],
      type: json['type']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nick_name": nick_name,
        "email": email,
        "image": image,
        "phone": phone,
        "ftoken": ftoken,
        "stream_token": stream_token,
        "phone_verified": phone_verified,
        "isFree": isFree,
        "type": type,
        "active": active,
      };
}
