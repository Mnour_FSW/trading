import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:trading/splash.dart';

// Pr Color
const storage = FlutterSecureStorage();

Color hexToColor(String code) {
  print('code:$code');
  return Color(int.parse(code));
}

class ColorController extends GetxController {
  // late Color myColors = myColor();
  @override
  void onInit() async {
    myColor();
    super.onInit();
  }

  myColor() async {
    String? ccc = await storage.read(key: "color");
    // Color colorApp = hexToColor(ccc!);
    // return colorApp;
  }
}

const int kPr = 0xff71c93e;

// Red Colors
// const KRed1 = Color(0xffB71C1C);
// const KRed2 = Color(0xffC62828);
// const KRed3 = Color(0xffD32F2F);
// const KRed4 = Color(0xffE53935);
const kRedDark = 0xff901313;
const kRed1 = 0xffB71C1C;
const kRed2 = 0xffC62828;
const kRed4 = 0xffE53935;
const kRed3 = 0xffD32F2F;

// Purple Colors
const KPurpleDark = 0xff340C65;
const KPurple1 = 0xff4A148C;
const KPurple2 = 0xff6A1B9A;
const KPurple3 = 0xff7B1FA2;
const KPurple4 = 0xff8E24AA;

// Blue Colors
const KBlueDark = 0xff0B3D89;
const KBlue4 = 0xff1E88E5;
const KBlue3 = 0xff1976D2;
const KBlue2 = 0xff1565C0;
const KBlue1 = 0xff0D47A1;

// Green Colors
const KTealDark = 0xff0D4111;
const KTeal4 = 0xff43A047;
const KTeal3 = 0xff388E3C;
const KTeal2 = 0xff2E7D32;
const KTeal1 = 0xff1B5E20;

// Gray Colors
const KGrayDark = 0xff0D1519;
const KGray4 = 0xff546E7A;
const KGray3 = 0xff455A64;
const KGray2 = 0xff37474F;
const KGray1 = 0xff263238;
