// To parse this JSON data, do
//
//     final notes = notesFromJson(jsonString);

import 'dart:convert';

List<Notes> notesFromJson(String str) =>
    List<Notes>.from(json.decode(str).
  map((x) => Notes.fromJson(x)));

String notesToJson(List<Notes> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Notes {
  Notes({
    required this.id,
    required this.userId,
    required this.title,
    required this.note,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int userId;
  String title;
  String note;
  DateTime createdAt;
  DateTime updatedAt;

  factory Notes.fromJson(Map<String, dynamic> json) => Notes(
        id: json["id"],
        userId: json["user_id"],
        title: json["title"],
        note: json["note"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "title": title,
        "note": note,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
