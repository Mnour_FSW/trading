// To parse this JSON data, do
//
//     final market = marketFromJson(jsonString);

import 'dart:convert';

import 'package:trading/models/stock_model.dart';

List<Market> marketFromJson(String str) => List<Market>.from(json.decode(str).map((x) => Market.fromJson(x)));

String marketToJson(List<Market> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Market {
    Market({
       required this.id,
       required this.name,
       required this.image,
       required this.code,
       required this.createdAt,
       required this.updatedAt,
      

    });

    int id;
    String name;
    String image;
    String code;
    DateTime createdAt;
    DateTime updatedAt;
  

    factory Market.fromJson(Map<String, dynamic> json) => Market(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        code: json["code"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      
    
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "code": code,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
     
    };
}
