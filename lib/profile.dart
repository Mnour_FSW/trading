import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:get/get.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// class ProfileScreen extends StatelessWidget {
//   const ProfileScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Center(child: Text('Profile')),
//     );
//   }
// }

class ProfileScreen extends StatefulWidget {
  _Profile createState() => _Profile();
}

final storage = FlutterSecureStorage();

int selectedIndex = 3;

class _Profile extends State<ProfileScreen> {
  String? _phone;
  String? _userId;
  Future<void> getPhone() async {
    final storage = FlutterSecureStorage();
    final String? phone = await storage.read(key: "phone");
    final String? userId = await storage.read(key: "id");
    print(phone);
    setState(() {
      _userId = userId!;
      _phone = phone!;
    });
  }

  String? _userName;
  Future<void> getName() async {
    final storage = FlutterSecureStorage();
    final String? userName = await storage.read(key: "name");
    print(userName);

    setState(() => _userName = userName!);
  }

  @override
  void initState() {
    getName();
    getPhone();

    super.initState();
  }

  Widget build(BuildContext cx) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Stack(children: [
          Container(
            color: Colors.grey[200],
            child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Column(
                      children: [
                        Stack(
                            alignment: Alignment.bottomCenter,
                            overflow: Overflow.visible,
                            children: <Widget>[
                              Column(
                                children: [
                                  Stack(children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(0.0),
                                          bottomRight: Radius.circular(0.0),
                                        ),
                                        color: context.theme.primaryColor,
                                      ),
                                      height: 150,
                                    ),
                                    Positioned(
                                      right: 0,
                                      bottom: -50,
                                      top: -50,
                                      child: Opacity(
                                        opacity: 1,
                                        child: CustomPaint(
                                          size: const Size(150, 150),
                                          painter: CardShapePainter(
                                              40,
                                              context.theme.primaryColorDark,
                                              context.theme.primaryColor),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                              Positioned(
                                top: 60.0,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 140.0,
                                      width: 140.0,
                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 1,
                                              blurRadius: 20,
                                            ),
                                          ],
                                          shape: BoxShape.rectangle,
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(100.0),
                                          ),
                                          image: const DecorationImage(
                                            fit: BoxFit.cover,
                                            image: CachedNetworkImageProvider(
                                                'https://cdn-icons-png.flaticon.com/512/149/149071.png'),
                                          ),
                                          border: Border.all(
                                            color: Colors.white,
                                            width: 5.0,
                                          )),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Column(children: <Widget>[
                                      Text(
                                        "$_userName",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 25.0),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "رقم التعريف " + " $_userId",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black54,
                                            fontSize: 14.0),
                                      ),
                                      const SizedBox(
                                        height: 3,
                                      ),
                                      Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: Text(
                                          "$_phone",
                                          style: TextStyle(
                                              fontSize: 17.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey[600]),
                                        ),
                                      ),
                                    ]),
                                  ],
                                ),
                              ),
                            ]),
                        Padding(
                          padding: const EdgeInsets.only(top: 180.0),
                          child: Container(
                            margin: const EdgeInsets.only(top: 0),
                            width: 320,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                            ),
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed("editProfile");
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: const <Widget>[
                                      Icon(
                                        Icons.lock,
                                        color: Colors.grey,
                                        size: 25,
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'تعديل الملف الشخصي',
                                          style: TextStyle(fontSize: 16.0),
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  child: Opacity(
                                    opacity: 0.5,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed("myf");
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(
                                        Icons.payment,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'تجديد الإشتراك',
                                          style: TextStyle(fontSize: 16.0),
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  child: Opacity(
                                    opacity: 0.5,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed('invoices');
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(
                                        Icons.info_outline,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'ايصالات الدفع',
                                          style: TextStyle(fontSize: 16.0),
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  child: Opacity(
                                    opacity: 0.5,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed('colors');
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Icon(
                                        Icons.color_lens,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'تغير لون التطبيق',
                                          style: TextStyle(fontSize: 16.0),
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () async {
                            await storage.deleteAll();
                            Get.offAllNamed('login');
                          },
                          child: Container(
                            height: 50,
                            width: 320,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.power_settings_new,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text(
                                      'تسجيل خروج',
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class CardShapePainter extends CustomPainter {
  final double radius;
  final Color startColor;
  final Color endColor;

  CardShapePainter(this.radius, this.startColor, this.endColor);

  @override
  void paint(Canvas canvas, Size size) {
    var radius = 40.0;

    var paint = Paint();
    paint.shader = ui.Gradient.linear(
        Offset(0, 0), Offset(size.width, size.height), [
      HSLColor.fromColor(startColor).withLightness(0.8).toColor(),
      endColor
    ]);

    var path = Path()
      ..moveTo(0, size.height)
      ..lineTo(size.width - radius, size.height)
      ..quadraticBezierTo(
          size.width, size.height, size.width, size.height - radius)
      ..lineTo(size.width, radius)
      ..quadraticBezierTo(size.width, 0, size.width - radius, 0)
      ..lineTo(size.width - 1.5 * radius, 0)
      ..quadraticBezierTo(-radius, 2 * radius, 0, size.height)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
    //throw UnimplementedError();
  }
}
