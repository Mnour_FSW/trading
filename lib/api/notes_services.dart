import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:trading/addnote.dart';
import 'package:trading/controllers/notes_controller.dart';
import 'package:trading/models/notes_model.dart';

final storage = const FlutterSecureStorage();

class NotesService {
  static var client = http.Client();

  static Future<List<Notes>?> fetch_notes() async {
    String? userId = await storage.read(key: "id");
    try {
      var response = await http.Client()
          .get(Uri.parse('http://www.trading.algorexe.com/api/notes/$userId'));

      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        var jsonString = response.body;
        return notesFromJson(jsonString);
      }
    } finally {
      client.close();
    }
  }

  static removenotes({required id}) async {
    final storage = FlutterSecureStorage();
    final String? token = await storage.read(key: "token");
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/deleteNote"),
        body: {"note_id": id.toString(), "token": token});

    print(response.body);
    if (response.statusCode == 200) {
    } else {
      return null;
    }
  }

  // ignore: non_constant_identifier_names
  static addnote(String note, String title) async {
    final storage = FlutterSecureStorage();
    final String? token = await storage.read(key: "token");
    final String? id = (await storage.read(key: "id"));

    var response = await http.Client()
        .post(Uri.parse("http://www.trading.algorexe.com/api/addNote"), body: {
      "token": token,
      "user_id": id.toString(),
      "title": title,
      "note": note
    });

    if (response.statusCode == 200) {
    } else {
      return null;
    }
  }
}
