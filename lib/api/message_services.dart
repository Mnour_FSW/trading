import 'package:http/http.dart' as http;
import 'package:trading/models/chat_model.dart';
import 'package:trading/models/market_model.dart';
import 'package:trading/models/news_model.dart';
import 'package:trading/models/stock_model.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = const FlutterSecureStorage();

class MessageService {
  static var client = http.Client();

  static send_message(String massage, String chatId) async {
    String? userId = await storage.read(key: "id");

    try {
      var response = await http.Client().post(
          Uri.parse("http://www.trading.algorexe.com/api/addMassage"),
          body: {
            "user_id": userId,
            "chat_id": chatId,
            "massage": massage,
            "image": "image",
          });

      var jsonString = response.body;
      print(response.body);
    } finally {
      client.close();
    }
  }
}
