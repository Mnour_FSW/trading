import 'package:http/http.dart' as http;

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:trading/models/invoices_model.dart';

class InvoicesService {
  static var client = http.Client();
//  static String server = Uri.parse('http://www.trading.algorexe.com/api/market');

  static Future<List<Invoices>> fetch_invoices() async {
    try {
      final storage = FlutterSecureStorage();
      final String? userId = await storage.read(key: "id");
      var response = await http.Client().get(
          Uri.parse('http://www.trading.algorexe.com/api/invoices/$userId'));

      var jsonString = response.body;
      print(response.body);
      return invoicesFromJson(jsonString);
    } finally {
      client.close();
    }
  }
}
