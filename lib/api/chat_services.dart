import 'package:http/http.dart' as http;
import 'package:trading/models/chat_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = const FlutterSecureStorage();

class ChatService {
  static var client = http.Client();

  static Future<List<Chats>?> fetch_chats() async {
    var userId = await storage.read(key: "id");
    var userType = await storage.read(key: "type");

    try {
      print("user id " + userId.toString() + "stock id " + userType.toString());
      var response = await http.Client().post(
          Uri.parse("http://www.trading.algorexe.com/api/chats"),
          body: {"user_id": userId, "user_type": userType.toString()});
      print(response.body);
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        var jsonString = response.body;
        print(response.body.toString());
        return chatsFromJson(jsonString);
      }else {
        List<Chats>? x;
        return x;
      }
      // print(response.body);

    } finally {
      client.close();
    }
  }
}
