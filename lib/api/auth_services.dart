import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:trading/login.dart';
import 'package:trading/models/user.dart';

final FirebaseMessaging messaging = FirebaseMessaging.instance;
final ftoken = messaging.getToken().then((value) => print(value));

class AuthServices {
  static String baseApi = "http://www.trading.algorexe.com/api";
  static var client = http.Client();

  static register({
    required name,
    required nickName,
    required email,
    required phone,
    required password,
    required confirmPassword,
    required ftoken,
  }) async {
    var response = await client.post(
      Uri.parse(baseApi + "/register"),
      body: {
        "name": name,
        "nick_name": nickName,
        "email": email,
        "phone": phone,
        "password": password,
        "password_confirmation": confirmPassword,
        "token": ftoken,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      var stringObject = response.body;
      var user = userFromJson(stringObject);
      print("hello" + user.toString());
      return user;
    } else {
      Map<String, dynamic> res = jsonDecode(response.body);
      print('Howdy, ${res['error']}!');

      // Get.snackbar('Register', ' jjj');
      final myError = res['error'].toString();
      final without = myError.replaceAll(
          RegExp(
            r'(?:_|[^\w\s])+',
          ),
          '\n');
      Get.snackbar('Register', '$without');

      print(response.body);
    }
  }

  static otp({
    phone,
    code,
  }) async {
    var response = await client.post(
      Uri.parse(baseApi + "/phoneVerfiction"),
      body: {
        "phone": phone,
        "code": code,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      final storage = const FlutterSecureStorage();
      var stringObject = response.body;
      Map<String, dynamic> data = jsonDecode(stringObject);
      print('data: ${data['success']}');
      if (data['success'] == true) {
        print('phone done');
        await storage.write(key: "phone_verified", value: '1');
        return Get.offAllNamed('home');
      } else {
        return false;
      }
      print("hello" + response.body.toString());
    } else {
      print(response.body);
      return false;
    }
  }

  static login({required email, password}) async {
    var response = await client.post(Uri.parse("$baseApi/login"),
        body: {"email": email, "password": password});
    if (response.statusCode == 200) {
      var stringObject = response.body;
      var user = userFromJson(stringObject);
      print(response.body);

      return user;
    } else {
      Map<String, dynamic> res = jsonDecode(response.body);
      print('Howdy, ${res['success']}!');

      Get.snackbar('login', "خطأ في البريد الإلكتروني أو كلمة السر ");
      return null;
    }
  }
}
