import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = FlutterSecureStorage();

final String? userToken = storage.read(key: "token").toString();

class SubsecripServices {
  static String baseApi = "http://www.trading.algorexe.com/api";
  static var client = http.Client();
  static subsecrip({
    required mfresponse,
  }) async {
    final String? userId = await storage.read(key: "id");
    final String? userToken = await storage.read(key: "token");
    print(userId);
    print(userToken);

    var response = await client.post(
      Uri.parse(baseApi + "/add_subsecription"),
      body: {
        "user_id": userId,
        "token": userToken,
        "mfresponse": mfresponse,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      var stringObject = response.body;
      // var user = userFromJson(stringObject);
      // print("hello" + user.toString());
      print("Mnb response subsecrib" + stringObject);
      await storage.write(key: "active", value: "1");
      Get.toNamed('home');
      return stringObject;
    } else {
      print(response.body);
    }
  }
}
