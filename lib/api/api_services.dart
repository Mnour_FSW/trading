// ignore_for_file: unnecessary_new
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart' as dio;
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:trading/models/courses_model.dart';
import 'package:trading/models/market_model.dart';
import 'package:trading/models/news_model.dart';
import 'package:trading/models/reco_model.dart';
import 'package:trading/models/stock_model.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ApiService {
  static var client = http.Client();
//  static String server = Uri.parse('http://www.trading.algorexe.com/api/market');

  static Future<List<Market>> fetch_markets() async {
    try {
      var response = await http.Client()
          .get(Uri.parse('http://www.trading.algorexe.com/api/market'));

      var jsonString = response.body;
      return marketFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static Future<List<Reco>> fetch_all_recos(arguments) async {
    try {
      final storage = FlutterSecureStorage();
      final String? isFree = await storage.read(key: "isFree");
      var response = await http.Client().get(Uri.parse(
          'http://www.trading.algorexe.com/api/allRec/' +
              arguments +
              "/" +
              isFree!));

      var jsonString = response.body;
      // print('bjsnkjfd,'+response.body);
      return recoFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static Future<List<Reco>?> fetch_recos(arguments) async {
    try {
      var response = await http.Client().get(
          Uri.parse('http://www.trading.algorexe.com/api/newRec/' + arguments));

      if (response.statusCode == 200) {
        print(response.body);
        var jsonString = response.body;
        return recoFromJson(jsonString);
      } else {
        List<Reco>? x;
        return x;
      }

      print(response.body);
    } finally {
      client.close();
    }
  }

  static Future<List<Stock>> fetch_stocks(arguments) async {
    try {
      final storage = FlutterSecureStorage();
      final String? userId = await storage.read(key: "id");
      print(userId);
      var response = await http.Client().get(Uri.parse(
          'http://www.trading.algorexe.com/api/stocks/' +
              arguments +
              "/$userId"));

      var jsonString = response.body;
      print(response.body);
      return stockFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static Future<List<Stock>> fetch_all_stocks() async {
    try {
      final storage = FlutterSecureStorage();
      final String? userId = await storage.read(key: "id");
      print(userId);
      var response = await http.Client().get(Uri.parse(
          'http://www.trading.algorexe.com/api/allStocks' + "/$userId"));

      var jsonString = response.body;
      print(response.body);
      return stockFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static Future<List<Stock>?> fetch_all_favorites() async {
    try {
      final storage = FlutterSecureStorage();
      final String? userId = await storage.read(key: "id");
      print(userId);
      var response = await http.Client().get(Uri.parse(
          'http://www.trading.algorexe.com/api/favorite' + "/$userId"));

      if (response.statusCode == 200) {
        var jsonString = response.body;

        return stockFromJson(jsonString);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        List<Stock>? x;

        return x;
      }
    } finally {
      client.close();
    }
  }

  static Future<List<News>> fetch_news() async {
    try {
      var response = await http.Client()
          .get(Uri.parse('http://www.trading.algorexe.com/api/news/'));

      var jsonString = response.body;
      print(response.body);
      return newsFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static Future<List<Courses>> fetch_courses() async {
    try {
      var response = await http.Client()
          .get(Uri.parse('http://www.trading.algorexe.com/api/cources/'));

      var jsonString = response.body;
      print(response.body);
      return coursesFromJson(jsonString);
    } finally {
      client.close();
    }
  }

  static postview({required id}) async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    print("user id " + userId.toString() + "stock id " + id.toString());
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/viewsCount"),
        body: {"user_id": userId, "stock_id": id.toString()});
    print("Done response" + response.body);
    if (response.statusCode == 200) {
      var stringObject = response.body;

      print("true response" + response.body);
    } else {
      print("false response" + response.body);
      return null;
    }
  }

  static addtofavorite({required id}) async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    print("user id " + userId.toString() + "stock id " + id.toString());
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/addFavorite"),
        body: {"user_id": userId, "stock_id": id.toString()});
    print("Done response addFavorite" + response.body);
    if (response.statusCode == 200) {
      var stringObject = response.body;

      print("true response" + response.body);
    } else {
      print("false response" + response.body);
      return null;
    }
  }

  static removefavorite({required id}) async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    // print("user id " + userId.toString() + "stock id " + id.toString());
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/removeFavorite"),
        body: {"user_id": userId, "stock_id": id.toString()});
    // print("Done response removeFavorite" + response.body);
    if (response.statusCode == 200) {
      var stringObject = response.body;

      // print("true response" + response.body);
    } else {
      // print("false response" + response.body);
      return null;
    }
  }

  static editProfile(String? name, String? nickName, String? email) async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    if (name == null) {
      name = await storage.read(key: "name");
    }
    if (nickName == null) {
      nickName = await storage.read(key: "nickName");
    }
    if (email == null) {
      email = await storage.read(key: "email");
    }
    print("user id " + userId.toString());
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/editProfile"),
        body: {
          "user_id": userId,
          "name": name,
          "nickName": nickName,
          "email": email,
        });
    print("Done response addFavorite" + response.body);
    if (response.statusCode == 200) {
      await storage.write(key: "nickName", value: nickName);
      await storage.write(key: "name", value: name);
      await storage.write(key: "email", value: email);

      print("true response" + response.body);
    } else {
      print("false response" + response.body);
      return null;
    }
  }

  static resendCode() async {
    final storage = FlutterSecureStorage();
    final String? phone = await storage.read(key: "phone");
    print("user id " + phone.toString());
    var response = await http.Client().post(
        Uri.parse("http://www.trading.algorexe.com/api/resendCode"),
        body: {"phone": phone});
    print("Done response addFavorite" + response.body);
    if (response.statusCode == 200) {
      print("true response" + response.body);
    } else {
      print("false response" + response.body);
      return null;
    }
  }

  static editImageProfile(image) async {
    final Dio client;
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");

    //   FormData data;
    //     data = FormData.fromMap({
    //   "user_id": int.parse(userId!),

    //  "image": await MultipartFile.fromFile(image!.path),
    // });
    // print(image);
    // var postUri = Uri.parse('http://www.trading.algorexe.com/api/editProfileImage');
    // var request = new http.MultipartRequest("POST", postUri);
    // request.fields['user_id'] = userId;
    // request.files.add(new http.MultipartFile.fromBytes('image', await File.fromUri(Uri.parse(image.path)).readAsBytes()));

    // request.send().then((response) {
    //   response.stream.transform(utf8.decoder).listen((value) {
    //     print(value);
    //   });

    // }).catchError((e) {
    //   print(e);

    // });

    // var response = await http.Client().post(
    //     Uri.parse("http://www.trading.algorexe.com/api/editProfileImage"),
    //     data:{data}

    //     );

    // if (response.statusCode == 200) {
    //   // await storage.write(key: "nickName", value: nickName);

    //   print("true response" + response.body);
    // } else {
    //   print("false response" + response.body);
    //   return null;
    // }

    try {
      var dioRequest = dio.Dio();
      // dioRequest.options.baseUrl = '<YOUR-URL>';

      //[2] ADDING TOKEN
      dioRequest.options.headers = {
        // 'Authorization': '<IF-YOU-NEED-ADD-TOKEN-HERE>',
        'Content-Type': 'application/x-www-form-urlencoded'
      };

      //[3] ADDING EXTRA INFO
      var formData = new dio.FormData.fromMap({'user_id': userId});

      //[4] ADD IMAGE TO UPLOAD
      var file = await dio.MultipartFile.fromFile(image.path
          // filename: basename(image.path),
          // contentType: MediaType("image", basename(image.path))
          );

      formData.files.add(MapEntry('image', file));

      //[5] SEND TO SERVER
      var response = await dioRequest.post(
        "http://www.trading.algorexe.com/api/editProfileImage",
        data: formData,
      );
      // print(response.toString());
      await storage.write(key: "image", value: response.toString());

      // final result = json.decode(response.toString())['result'];
    } catch (err) {
      print('ERROR  $err');
    }
  }
}
