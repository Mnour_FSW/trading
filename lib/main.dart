import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:stream_chat_flutter/stream_chat_flutter.dart';
import 'package:stream_chat_persistence/stream_chat_persistence.dart';
import 'package:trading/addnote.dart';
import 'package:trading/all_cources.dart';
import 'package:trading/all_news.dart';
import 'package:trading/all_stocks.dart';
import 'package:trading/change_colors.dart';
import 'package:trading/chat.dart';
import 'package:trading/all_reco.dart';
import 'package:trading/controllers/favorite_controller.dart';
import 'package:trading/controllers/stock_controller.dart';
import 'package:trading/edit_prfile.dart';

import 'package:trading/notes.dart';
import 'package:trading/favorite.dart';
import 'package:trading/home.dart';
import 'package:trading/invoices.dart';
import 'package:trading/login.dart';
import 'package:trading/massages.dart';
import 'package:trading/my_fatoorah.dart';
import 'package:trading/otp.dart';
import 'package:trading/privacy_policy.dart';
import 'package:trading/profile.dart';
import 'package:trading/reco.dart';
import 'package:trading/registration.dart';
import 'package:trading/sahim.dart';
import 'package:trading/search.dart';
import 'package:trading/shaim_image.dart';
import 'package:trading/souk.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:trading/splash.dart';
import 'bindings/login_binding.dart';
import 'bindings/register_binding.dart';
import 'models/colors.dart';
import 'news.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    // description
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  final storage = const FlutterSecureStorage();
  String? themeIndex = await storage.read(key: 'themeIndex');
  int index;
  if (themeIndex == null) {
    index = 0;
  } else {
    index = int.parse(themeIndex);
  }

  final savedThemeMode = await AdaptiveTheme.getThemeMode();
  runApp(MyApp(savedThemeMode: savedThemeMode, themeIndex: index));
}

class MyApp extends StatelessWidget {
  final AdaptiveThemeMode? savedThemeMode;
  final int? themeIndex;

  const MyApp({Key? key, this.savedThemeMode, required this.themeIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: Themes.change[themeIndex ?? 0],
      dark: ThemeData.dark(),
      initial: savedThemeMode ?? AdaptiveThemeMode.light,
      builder: (theme, darkTheme) => GetMaterialApp(
        //       title: 'Theme change using Get',
        theme: theme,
        darkTheme: darkTheme,
        // themeMode: Themes.custom,
        // NOTE: Optional - use themeMode to specify the startup theme
        // themeMode: ThemeMode.system,
        debugShowCheckedModeBanner: false,
        unknownRoute: GetPage(name: '/notfound', page: () => ProfileScreen()),
        initialRoute: '/',
        getPages: [
          GetPage(name: '/', page: () => const SplashScreen()),
          GetPage(
            name: '/login',
            page: () => LoginScreen(),
            binding: LoginBinding(),
          ),
          GetPage(
            name: '/reco',
            page: () => RecoScreen(),
          ),
          GetPage(
            name: '/sahim_image',
            page: () => SahimImage(),
          ),
          GetPage(
            name: '/allreco',
            page: () => AllRecoScreen(),
          ),
          GetPage(
            name: '/home',
            page: () => MyHomePage(),
          ),
          GetPage(
            name: '/soukscreen',
            page: () => SoukScreen(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/sahim',
            page: () => SahimScreen(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/registration',
            page: () => RegistrationScreen(),
            transition: Transition.fadeIn,
            binding: RegisterBinding(),
          ),
          GetPage(
            name: '/myf',
            page: () => const MyFatoora(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/news',
            page: () => const NewsScreen(),
          ),
          GetPage(
            name: '/favorite',
            page: () => FavoriteScreen(),
          ),
          GetPage(
            name: '/allstocks',
            page: () => AllStocks(),
          ),
          GetPage(
            name: '/search',
            page: () => SearchBar(),
          ),
          GetPage(
            name: '/privacy_policy',
            page: () => const PrivacyPolicy(),
          ),
          // GetPage(
          //   name: '/massages',
          //   page: () => StreamMessages(),
          //   transition: Transition.fadeIn,
          // ),

          GetPage(
            name: '/all_cources',
            page: () => AllCourses(),
            transition: Transition.fadeIn,
          ),

          GetPage(
            name: '/all_news',
            page: () => AllNews(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/invoices',
            page: () => InvoicesScreen(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/profile',
            page: () => ProfileScreen(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/editProfile',
            page: () => EditProfile(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/addNote',
            page: () => AddNote(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/notes',
            page: () => NoteScreen(),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/otp',
            page: () => PinCodeVerificationScreen('+8801376221100'),
            transition: Transition.fadeIn,
          ),
          GetPage(
            name: '/colors',
            page: () => const ChangeColors(),
            transition: Transition.fadeIn,
          ),
        ],
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // final StreamChatClient client;
  // final Channel channel;
  const MyHomePage({
    Key? key,
    // required this.client,
    // required this.channel,
  });

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _page = 2;
  final HomeScreen _homeScreen = HomeScreen();
  final FavoriteScreen _favoritecreen = FavoriteScreen();
  final AllStocks _allStocks = AllStocks();
  final NoteScreen _excelScreen = NoteScreen();
  final ProfileScreen _profileScreen = ProfileScreen();
  // list
  // final StreamMessages _chatScreen =  ;

  Widget _showPage = HomeScreen();

  Widget _pageChosser(int page) {
    switch (page) {
      case 0:
        return _favoritecreen;
      case 1:
        return _excelScreen;
      case 2:
        return _homeScreen;
      case 3:
        return _allStocks;
      case 4:
        return _profileScreen;
      default:
        return Center(
          child: Text('no $_page'),
        );
    }
  }

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        if (message.data['where'] == 'Reco') {
          FavoriteController favoriteController = Get.put(FavoriteController());

          favoriteController.changeViews(message.data['stock_code']);

          StockController stockController = Get.put(StockController());

          stockController.changeViews(message.data['stock_code']);
        }
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                color: context.theme.primaryColor,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      // print('A new onMessageOpenedApp event was published!');
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        // print('message.datamessage.datamessage.datamessage.datamessage.datamessage.datamessage.datamessage.datamessage.datamessage.data');
        print(message.data['where']);
        Get.toNamed(message.data['screen']);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.transparent,

        // buttonBackgroundColor: Color(0xff3C8B62),

        index: _page,
        //key: _bottomNavigationKey,
        items: <Widget>[
          Icon(Icons.star, size: 30, color: context.theme.primaryColor),
          Icon(Icons.note_add, size: 30, color: context.theme.primaryColor),
          Icon(Icons.home, size: 30, color: context.theme.primaryColor),
          Icon(Icons.list, size: 30, color: context.theme.primaryColor),
          Icon(Icons.person, size: 30, color: context.theme.primaryColor),
        ],
        onTap: (int tapIndex) {
          setState(() {
            _showPage = _pageChosser(tapIndex);
            setState(() {});
          });
        },
      ),
      body: Center(
        child: _showPage,
      ),
    );
  }
}








// class ThemeService {
//   final _box = GetStorage();
//   final _key = 'isDarkMode';
  
//   /// Get isDarkMode info from local storage and return ThemeMode
//   ThemeMode get theme => _loadThemeFromBox() ? ThemeMode.dark : ThemeMode.light;

//   /// Load isDArkMode from local storage and if it's empty, returns false (that means default theme is light)
//   bool _loadThemeFromBox() => _box.read(_key) ?? false;
  
//   /// Save isDarkMode to local storage
//   _saveThemeToBox(bool isDarkMode) => _box.write(_key, isDarkMode);
  
//   /// Switch theme and save to local storage
//   void switchTheme() {
//     Get.changeThemeMode(_loadThemeFromBox() ? ThemeMode.light : ThemeMode.dark);
//     _saveThemeToBox(!_loadThemeFromBox());
//   }
// }