import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:trading/controllers/register_controller.dart';
import 'package:get/get.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class RegistrationScreen extends GetView<RegisterController> {
  RegisterController ccontroller = Get.find();
  // final registerFormKey = GlobalKey<FormState>();

  bool invisible = true;

  @override
  Widget build(BuildContext context) {
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    messaging
        .getToken()
        .then((value) => controller.firbasetoken = value.toString());
    //final routes = ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    // final _formKey = GlobalKey<FormState>();
    final emailField = Container(
      height: 35,
      child: TextFormField(
        controller: controller.emailController,
        obscureText: false,
        validator: (v) {
          return controller.validateEmail(v!);
        },
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            suffixIcon: Icon(
              Icons.email_outlined,
              color: Colors.grey,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'البريد الإلكتروني',

            //
            labelStyle: TextStyle(
              color: Colors.white,
            )),
      ),
    );
    final phoneField = Container(
      height: 50,
      child: InternationalPhoneNumberInput(
        inputDecoration: const InputDecoration(
          contentPadding: EdgeInsets.all(4),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          hintText: 'رقم الهاتف',
          labelStyle: TextStyle(
            color: Colors.grey,
          ),
        ),
        selectorButtonOnErrorPadding: 0,
        errorMessage: 'رقم هاتف غير صحيح',
        textStyle: const TextStyle(color: Colors.grey),
        hintText: 'رقم الهاتف',
        onInputChanged: (PhoneNumber number) {
          print(number.phoneNumber);
          // PhoneNumber? numbers;
          // if(controller.phoneController.)
          controller.phoneController.text = number.toString();
          // controller.phoneController.text = number.phoneNumber.toString();
        },
        onInputValidated: (
          bool value,
        ) {
          // controller.phoneController.text = number.phoneNumber.toString();

          print(value.toString());
        },
        selectorConfig: SelectorConfig(
          selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
        ),
        ignoreBlank: false,
        autoValidateMode: AutovalidateMode.disabled,
        spaceBetweenSelectorAndTextField: 0,
        selectorTextStyle: TextStyle(fontSize: 16, color: Colors.grey),
        // initialValue: PhoneNumber(isoCode: 'LB'),
        // textFieldController: controller.phoneController,
        formatInput: false,
        keyboardType:
            TextInputType.numberWithOptions(signed: true, decimal: true),
        // inputBorder: OutlineInputBorder(),
        onSaved: (PhoneNumber number) {
          print('On Saved: $number');
          controller.phoneController.text = number.toString();

          //  controller.phoneController=number;
        },
      ),
      // child: TextFormField(
      //   controller: controller.phoneController,
      //   obscureText: false,
      //   keyboardType: TextInputType.number,
      //   style: TextStyle(
      //     color: Colors.white,
      //     decorationColor: Colors.white,
      //   ),
      //   cursorColor: Colors.black,
      //   decoration: InputDecoration(
      //       suffixIcon: Icon(
      //         Icons.phone,
      //         color: Colors.grey,
      //       ),
      //       enabledBorder: UnderlineInputBorder(
      //         borderSide: BorderSide(
      //           color: Colors.grey,
      //         ),
      //       ),
      //       border: UnderlineInputBorder(
      //           borderSide: BorderSide(color: Colors.white)),
      //       focusedBorder: UnderlineInputBorder(
      //         borderSide: BorderSide(
      //           color: Colors.grey,
      //         ),
      //       ),
      //       hintStyle: TextStyle(
      //         color: Colors.grey,
      //       ),
      //       hintText: 'رقم الهاتف',

      //       //
      //       labelStyle: TextStyle(
      //         color: Colors.white,
      //       )),
      // ),
    );
    final passwordField = Container(
      height: 35,
      child: Obx(
        () => TextFormField(
          obscureText: controller.isHidden.value,
          controller: controller.passwordController,
          validator: (v) {
            return controller.validatePassword(v!);
          },
          keyboardType: TextInputType.visiblePassword,
          style: const TextStyle(
            color: Colors.grey,
            decorationColor: Colors.grey,
          ),
          cursorColor: Colors.black,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            suffixIcon: InkWell(
              onTap: () {
                controller.isHidden.value = !controller.isHidden.value;
              },
              child: Icon(
                controller.isHidden.value
                    ? Icons.visibility
                    : Icons.visibility_off,
                color: Colors.grey,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'كلمة السر',
            // labelText: 'Password',
            // labelStyle: TextStyle(
            //   color: Colors.white,
            // )
          ),
        ),
      ),
    );
    final confirmPasswordField = Container(
      height: 35,
      child: Obx(
        () => TextFormField(
          obscureText: controller.isConfirmHidden.value,
          controller: controller.confirmPasswordController,
          validator: (v) {
            return controller.validatePassword(v!);
          },
          keyboardType: TextInputType.visiblePassword,
          style: TextStyle(
            color: Colors.grey,
            decorationColor: Colors.grey,
          ),
          cursorColor: Colors.black,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            suffixIcon: InkWell(
              onTap: () {
                controller.isConfirmHidden.value =
                    !controller.isConfirmHidden.value;
              },
              child: Icon(
                controller.isConfirmHidden.value
                    ? Icons.visibility
                    : Icons.visibility_off,
                color: Colors.grey,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'تأكيد كلمة السر',
          ),
        ),
      ),
    );

    final nameField = Container(
      height: 35,
      child: TextFormField(
        //obscureText: true,
        controller: controller.nameController,
        validator: (v) {},
        keyboardType: TextInputType.name,
        style: const TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(bottom: 5),
          suffixIcon: Icon(
            Icons.person,
            color: Colors.grey,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          hintText: 'الاسم الثلاثي',
        ),
      ),
    );
    final nickNameField = Container(
      height: 35,
      child: TextFormField(
        //obscureText: true,
        controller: controller.nickNameController,
        validator: (v) {},
        keyboardType: TextInputType.name,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(bottom: 5),
          suffixIcon: Icon(
            Icons.person,
            color: Colors.grey,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          hintText: 'الإسم المستعار',
          // labelText: 'Password',
          // labelStyle: TextStyle(
          //   color: Colors.white,
          // )
        ),
      ),
    );

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 150,
                            width: 150,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage('assets/images/mec.png'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      "إنشاء حساب",
                      style: TextStyle(
                          color: Color(0xff545454),
                          fontSize: 24,
                          fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: 22,
                    ),
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.6,
                          child: Column(
                            children: [
                              Container(
                                child: Form(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  key: controller.registerFormKey,
                                  child: Column(
                                    children: [
                                      nickNameField,
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      nameField,
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      phoneField,
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      emailField,
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      passwordField,
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      confirmPasswordField,
                                      const SizedBox(
                                        height: 45,
                                      ),
                                      Container(
                                        width: double.infinity,
                                        height: 45.0,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Color(0xff71c93e)),
                                        child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            onTap: () {
                                              controller.doRegister();
                                            },
                                            child: Center(
                                              child: Obx(
                                                () => controller
                                                            .isLoading.value ==
                                                        true
                                                    ? Center(
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              'تحميل',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 20,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                            const SizedBox(
                                                              width: 20,
                                                            ),
                                                            Container(
                                                              height: 20,
                                                              width: 20,
                                                              child:
                                                                  CircularProgressIndicator(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    : Text(
                                                        'إنشاء حساب',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .grey[800],
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Get.offNamed('login');
                                },
                                child: Text(
                                  "العودة لتسجيل الدخول",
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // Positioned(
            //   top: 0,
            //   bottom: 0,
            //   left: 0,
            //   right: 0,
            //   child: Container(
            //     color: Colors.white.withOpacity(0.85),
            //     //
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}

class Email extends StatelessWidget {
  const Email({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
            suffixIcon: Icon(
              Icons.email_outlined,
              color: Colors.grey,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'someone@email.com',
            //
            labelStyle: TextStyle(
              color: Colors.white,
            )),
      ),
    );
  }
}

class Password extends StatelessWidget {
  const Password({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
          suffixIcon: Icon(
            Icons.lock_outlined,
            color: Colors.grey,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          border:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          hintText: 'Password',
          // labelText: 'Password',
          // labelStyle: TextStyle(
          //   color: Colors.white,
          // )
        ),
      ),
    );
  }
}

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;
  final double width;
  final double height;
  final Function onPressed;

  const RaisedGradientButton({
    required Key key,
    required this.child,
    required this.gradient,
    this.width = double.infinity,
    this.height = 45.0,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 45.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: gradient,
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            // onTap: onPressed,
            // child: Center(
            //   child: child,
            // ),
            ),
      ),
    );
  }
}
