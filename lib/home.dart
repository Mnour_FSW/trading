import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:stream_chat_flutter/stream_chat_flutter.dart';
import 'package:trading/controllers/market_controller.dart';

import 'package:get/get.dart';

import 'package:trading/controllers/news_controller.dart';
import 'package:trading/stream_chat/message_config.dart';
import 'package:trading/stream_chat/newmessages.dart';
import 'package:trading/widgets/cources_videos_widget.dart';
import 'package:trading/widgets/news_widgets.dart';

import 'controllers/courses_controller.dart';
import 'models/colors.dart';
import 'widgets/side_drawer.dart';

class HomeScreen extends StatelessWidget {
  final client = StreamChatClient(streamKey);

  Future initClient() async {
    const storage = FlutterSecureStorage();
    String? userId = await storage.read(key: "id");
    String? stream_token = await storage.read(key: 'stream_token');
    String? token = await storage.read(key: 'token');
    //           client.chatPersistenceClient = StreamChatPersistenceClient(
    //   logLevel: Level.ALL,
    //   connectionMode: ConnectionMode.background,
    // );

    await client.connectUser(
        User(id: userId.toString()), stream_token.toString());
    // client.addDevice(token.toString(), PushProvider.firebase);
    print('hellooooooooooo');

    //  final channelsm = client.channel('messaging',id:'public_gorup');
    //   await channelsm.watch();
  }

  final MarketController marketController = Get.put(MarketController());
  // final ColorsController ccolor = Get.put(ColorsController());

  final NewsController newsController = Get.put(NewsController());
  final CoursesController coursesController = Get.put(CoursesController());
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  // int _currentPage = 0;
  @override
  Widget build(BuildContext context) {
    int _page = 0;
    GlobalKey _bottomNavigationKey = GlobalKey();

//  if(!StreamChat.of(context).currentUser!.online == true){
    initClient();

//  }

    return StreamChat(
      client: client,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          key: _key, //
          drawer: SideDrawer(),
          backgroundColor: Colors.grey[200],
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            bottom: PreferredSize(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: GestureDetector(
                  onTap: () {
                    Get.toNamed('search');
                  },
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.search,
                          color: Colors.grey[500],
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text("بحث",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[500],
                            )),
                      ],
                      // initialValue: "بحث",
                      // style: TextStyle(fontSize: 30),
                      // focusedBorder: UnderlineInputBorder(
                      //     borderSide: BorderSide(color: Colors.cyan),
                      //  ),

                      // decoration: InputDecoration(
                      //   icon: Icon(Icons.search),
                      //   enabledBorder: UnderlineInputBorder(
                      //     borderSide: BorderSide(color: Colors.cyan),
                      //   ),
                      //   contentPadding:
                      //       EdgeInsets.symmetric(horizontal: 0, vertical: 1),
                      // ),
                    ),
                  ),
                ),
              ),
              preferredSize: Size(50, 50),
            ),
            toolbarHeight: 50,
            backgroundColor: Colors.transparent.withOpacity(0),
            actions: [
              SizedBox(
                width: 50,
              ),
            ],
            leading: InkWell(
              onTap: () {
                _key.currentState!.openDrawer();
              },
              child: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
            elevation: 0,
            title: const Center(
              child: Text(
                "MARKET EXPERTS CLUB ",
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
          body: Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: [
                Positioned(
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 260,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 1.9,
                                  child: PageView(
                                    physics: ClampingScrollPhysics(),
                                    controller: _pageController,
                                    onPageChanged: (int page) {},
                                    children: <Widget>[
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text('',
                                                style: TextStyle(fontSize: 20)),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              StreamMessages(
                                                                client: client,
                                                              )),
                                                    );
                                                    // Get.toNamed('allstocks');
                                                  },
                                                  child: Container(
                                                    height: 60,
                                                    width:
                                                        (MediaQuery.of(context)
                                                                    .size
                                                                    .width /
                                                                2) -
                                                            35,
                                                    margin:
                                                        const EdgeInsets.only(
                                                            bottom: 20,
                                                            left: 2,
                                                            right: 2),
                                                    padding:
                                                        const EdgeInsets.all(
                                                            15),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20),
                                                      color: Colors.white,
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey
                                                              .withOpacity(0.4),
                                                          spreadRadius: 1,
                                                          blurRadius: 2,
                                                          offset: Offset(0, 0),
                                                        ),
                                                      ],
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        Icon(
                                                          Icons.chat,
                                                          size: 30,
                                                          color: context.theme
                                                              .primaryColor,
                                                        ),
                                                        const SizedBox(
                                                          width: 15,
                                                        ),
                                                        Text(
                                                          'الدردشات',
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: Colors
                                                                  .grey[800]),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 20,
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    //favorite
                                                    Get.toNamed('favorite');
                                                  },
                                                  child: Container(
                                                    height: 60,
                                                    width:
                                                        (MediaQuery.of(context)
                                                                    .size
                                                                    .width /
                                                                2) -
                                                            36,
                                                    margin:
                                                        const EdgeInsets.only(
                                                            bottom: 20,
                                                            left: 2,
                                                            right: 2,
                                                            top: 2),
                                                    padding: EdgeInsets.all(15),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20),
                                                      color: Colors.white,
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey
                                                              .withOpacity(0.4),
                                                          spreadRadius: 1,
                                                          blurRadius: 2,
                                                          offset: Offset(0, 0),
                                                        ),
                                                      ],
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        const Icon(
                                                          Icons.star,
                                                          size: 30,
                                                          color:
                                                              Color(0xffFFD700),
                                                        ),
                                                        const SizedBox(
                                                          width: 15,
                                                        ),
                                                        Text(
                                                          'المفضلة',
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: Colors
                                                                  .grey[800]),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Expanded(
                                              child: Obx(() {
                                                if (marketController
                                                    .isLoading.value) {
                                                  return const Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  );
                                                } else {
                                                  return ListView.builder(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              0.0),
                                                      physics:
                                                          const NeverScrollableScrollPhysics(),
                                                      itemCount:
                                                          marketController
                                                              .marketList
                                                              .length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return SoukCard(
                                                          name: marketController
                                                              .marketList[index]
                                                              .name,
                                                          code: marketController
                                                              .marketList[index]
                                                              .code,
                                                          id: marketController
                                                              .marketList[index]
                                                              .id,
                                                          image:
                                                              'http://www.trading.algorexe.com/images/${marketController.marketList[index].image}',
                                                        );
                                                      });
                                                }
                                              }),
                                            )
                                          ],
                                        ),
                                      ),
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                  child: Text('',
                                                      style: TextStyle(
                                                          fontSize: 20))),
                                              GestureDetector(
                                                onTap: () {
                                                  Get.toNamed('all_cources');
                                                },
                                                child: Text('مشاهدة الكل',
                                                    style: TextStyle(
                                                        fontSize: 14)),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Expanded(
                                            child: Obx(() {
                                              if (coursesController
                                                  .isLoading.value) {
                                                return const Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                );
                                              } else {
                                                return ListView.builder(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            0.0),
                                                    itemCount: coursesController
                                                        .coursesList.length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return CourcesVideo(
                                                        url: coursesController
                                                            .coursesList[index]
                                                            .link,
                                                        title: coursesController
                                                            .coursesList[index]
                                                            .title,
                                                      );
                                                    });
                                              }
                                            }),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                    child: Text('',
                                                        style: TextStyle(
                                                            fontSize: 20))),
                                                GestureDetector(
                                                  onTap: () {
                                                    Get.toNamed('all_news');
                                                  },
                                                  child: Text('مشاهدة الكل',
                                                      style: TextStyle(
                                                          fontSize: 14)),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                              ],
                                            ),
                                            //news
                                            const SizedBox(
                                              height: 15,
                                            ),
                                            Expanded(
                                              child: Obx(() {
                                                if (newsController
                                                    .isLoading.value) {
                                                  return const Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  );
                                                } else {
                                                  return ListView.builder(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              0.0),
                                                      itemCount: newsController
                                                          .newsList.length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return GestureDetector(
                                                          onTap: () {
                                                            Get.toNamed(
                                                              'news?id=${newsController.newsList[index].id}&title=${newsController.newsList[index].title}&image=${newsController.newsList[index].image}&articel=${newsController.newsList[index].articel}&date=${newsController.newsList[index].createdAt}',
                                                            );
                                                          },
                                                          child: NewWidget(
                                                            id: newsController
                                                                .newsList[index]
                                                                .id,
                                                            title:
                                                                newsController
                                                                    .newsList[
                                                                        index]
                                                                    .title,
                                                            image:
                                                                newsController
                                                                    .newsList[
                                                                        index]
                                                                    .image,
                                                            articel:
                                                                newsController
                                                                    .newsList[
                                                                        index]
                                                                    .articel,
                                                            date: newsController
                                                                .newsList[index]
                                                                .createdAt
                                                                .toString(),
                                                          ),
                                                        );
                                                      });
                                                }
                                              }),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    // width: 2000,
                    // height: 200,
                    height: 220,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(50),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          context.theme.primaryColor,
                          context.theme.primaryColorDark,
                        ],
                      ),
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(70),
                          bottomLeft: Radius.circular(70)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.4),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0, 0))
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  child: Container(
                    height: 360,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            // _pageController.nextPage(
                            //     duration: Duration(milliseconds: 100),
                            //     curve: Curves.ease);
                            _pageController.jumpToPage(0);
                            marketController.currentPage.value = 0;
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: 160,
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.only(
                                      left: 20, top: 20, right: 20, bottom: 10),
                                  width: 120,
                                  height: 110,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 2,
                                            offset: Offset(0, 0))
                                      ],
                                    ),
                                    child: Obx(
                                      () => Icon(
                                        Icons.list,
                                        color: marketController
                                                    .currentPage.value ==
                                                0
                                            ? context.theme.primaryColor
                                            : Colors.grey,
                                        size: 35,
                                      ),
                                    ),
                                  ),
                                ),
                                const Text(
                                  'استشارات الأسواق',
                                )
                              ],
                            ),
                          ),
                        ),
                        // padding: EdgeInsets.only(left: 20, right: 20, top: 40),
                        GestureDetector(
                          onTap: () {
                            _pageController.jumpToPage(1);
                            print(marketController.currentPage.value);
                            marketController.currentPage.value = 1;
                            print(marketController.currentPage.value);
                          },
                          child: Container(
                            padding: EdgeInsets.only(top: 20),
                            width: MediaQuery.of(context).size.width / 3,
                            height: 160,
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: 20, top: 20, right: 20, bottom: 10),
                                  width: 120,
                                  height: 110,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 2,
                                            offset: Offset(0, 0))
                                      ],
                                    ),
                                    child: Obx(
                                      () => Icon(
                                        Icons.import_contacts,
                                        size: 35,
                                        color: marketController
                                                    .currentPage.value ==
                                                1
                                            ? context.theme.primaryColor
                                            : Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'دروس تعليمية',
                                )
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            marketController.currentPage.value = 2;
                            _pageController.jumpToPage(2);
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: 160,
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: 20, top: 20, right: 20, bottom: 10),
                                  width: 120,
                                  height: 110,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 2,
                                            offset: Offset(0, 0))
                                      ],
                                    ),
                                    child: Obx(
                                      () => Icon(
                                        Icons.library_books,
                                        color: marketController
                                                    .currentPage.value ==
                                                2
                                            ? context.theme.primaryColor
                                            : Colors.grey,
                                        size: 35,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'اخر الأخبار',
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SoukCard extends StatelessWidget {
  final String name;
  final int id;
  final String code;
  //final rout;
  final String image;
  const SoukCard({
    Key? key,
    required this.name,
    required this.id,
    required this.code,
    //required this.rout,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      height: 100,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 1,
            blurRadius: 2,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              Get.toNamed(
                'soukscreen',
                arguments: '${id}',
              );
            },
            child: Container(
              width: 70,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: CachedNetworkImageProvider(image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(15),
                border: Border.all(color: Colors.grey),
                // border:
                //     Border.all(color: Colors.blueAccent),
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '${code} - ${name}',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[800]),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.toNamed('reco', arguments: '${id}');
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: context.theme.primaryColor),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          Text(
                            'استشارات جديدة',
                            style: TextStyle(fontSize: 12),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border:
                                  Border.all(color: context.theme.primaryColor),
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed('allreco', arguments: '$id');
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: context.theme.primaryColor),
                        color: Colors.white,
                      ),
                      child: const Text(
                        'كل الاستشارات',
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
