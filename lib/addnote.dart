import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/models/notes_model.dart';

import 'controllers/notes_controller.dart';

class AddNote extends StatelessWidget {
  const AddNote({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('إضافة ملاحظة جديدة'),
          backgroundColor: context.theme.primaryColor,
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  late String? title;
  late String? note;
  final NotesController notesController = Get.put(NotesController());

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('العنوان'),
                TextFormField(
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'الرجاء أدخل عنوان الملاحظة';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    title = value;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text('الملاحظة'),
                TextFormField(
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'الرجاء أدخل الملاحظة';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    note = value;
                  },
                  maxLines: null,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(context.theme.primaryColor),
                    ),
                    onPressed: () {
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_formKey.currentState!.validate()) {
                        Notes noteNew = Notes(
                            createdAt: DateTime.now(),
                            id: 20,
                            note: note!,
                            title: title!,
                            userId: 2,
                            updatedAt: DateTime.now());

                        notesController.addNote(noteNew);
                        // Timer(const Duration(milliseconds: 2000), () {

                        //     notesController.fetchnotes();

                        // });

                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        // ScaffoldMessenger.of(context).showSnackBar(
                        //   const SnackBar(content: Text('Processing Data')),

                        //   // NotesService.Addnote( note: note, title: title);
                        // );
                        Get.back();
                      }
                    },
                    child: const Text('حفظ'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
