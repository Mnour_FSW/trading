import 'dart:async';
import 'package:algolia/algolia.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/widgets/souk_search_widgets.dart';

import 'controllers/stock_controller.dart';

class AlgoliaApplication {
  static final Algolia algolia = Algolia.init(
    applicationId: 'CPPW9ZKM3F', //ApplicationID
    apiKey:
        '11fb4ca683b394ed7c84c976fc2cff74', //search-only api key in flutter code
  );
}

class SearchBar extends StatefulWidget {
  SearchBar({Key? key}) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final Algolia _algoliaApp = AlgoliaApplication.algolia;
  String? _searchTerm = "";
  final StockController stockController = Get.put(StockController());

  Future<List<AlgoliaObjectSnapshot>> _operation(String? input) async {
    AlgoliaQuery query = _algoliaApp.instance.index("stocks").query(input!);
    AlgoliaQuerySnapshot querySnap = await query.getObjects();
    List<AlgoliaObjectSnapshot> results = querySnap.hits;
    print(results);
    return results;
    // ignore: avoid_print
  }

  @override
  Widget build(BuildContext context) {
    //  stockController.fetchstock(Get.parameters['code']);
    return SafeArea(
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              "البحث",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: context.theme.primaryColor,
          ),
          body: SingleChildScrollView(
            child: Column(children: <Widget>[
              TextField(
                  onChanged: (val) {
                    setState(() {
                      _searchTerm = val;
                    });
                  },
                  style: const TextStyle(color: Colors.black, fontSize: 20),
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: 'ابحث بواسطة الأسم أو الكود',
                      hintStyle: TextStyle(color: Colors.black),
                      prefixIcon: Icon(Icons.search, color: Colors.black))),
              SingleChildScrollView(
                child: Column(
                  children: [
                    StreamBuilder<List<AlgoliaObjectSnapshot>>(
                      stream: Stream.fromFuture(_operation(_searchTerm)),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return const Text(
                            "اكتب الأن",
                            style: TextStyle(color: Colors.black),
                          );
                        } else {
                          List<AlgoliaObjectSnapshot>? currSearchStuff =
                              snapshot.data;

                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Container();
                            default:
                              if (snapshot.hasError)
                                return Text('Error: ${snapshot.error}');
                              else {
                                return CustomScrollView(
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverList(
                                      delegate: SliverChildBuilderDelegate(
                                        (context, index) {
                                          return _searchTerm!.isNotEmpty
                                              ? GestureDetector(
                                                  onTap: () {
                                                    Get.toNamed(
                                                      'sahim?id=${currSearchStuff![index].data["id"]}&code=${currSearchStuff[index].data["code"]}&name=${currSearchStuff[index].data["name"]}&image=${'http://www.trading.algorexe.com/images/${currSearchStuff[index].data["image"]}'}',
                                                    );
                                                  },
                                                  child: SoukWidgetsCard(
                                                      id: currSearchStuff![
                                                              index]
                                                          .data["id"],
                                                      name:
                                                          currSearchStuff[index]
                                                              .data["name"],
                                                      code:
                                                          currSearchStuff[index]
                                                              .data["code"],
                                                      image:
                                                          "http://www.trading.algorexe.com/images/" +
                                                              currSearchStuff[
                                                                          index]
                                                                      .data[
                                                                  "image"]),
                                                )

                                              // ? DisplaySearchResult(
                                              //     name: currSearchStuff![index]
                                              //         .data["name"],
                                              //     code: currSearchStuff[index]
                                              //         .data["code"],
                                              //   )
                                              : Container();
                                        },
                                        childCount: currSearchStuff!.length,
                                      ),
                                    ),
                                  ],
                                );
                              }
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class DisplaySearchResult extends StatelessWidget {
  final String? name;
  final String? code;

  DisplaySearchResult({
    Key? key,
    this.name,
    this.code,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text(
        name ?? "",
        style: TextStyle(color: Colors.black),
      ),
      Text(
        code ?? "",
        style: TextStyle(color: Colors.black),
      ),
      Divider(
        color: Colors.black,
      ),
      SizedBox(height: 20)
    ]);
  }
}
