import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:get/get.dart';
import 'package:trading/models/courses_model.dart';

class CoursesController extends GetxController {
  var isLoading = true.obs;
  var coursesList = <Courses>[].obs;

  // NewsController(arguments);
  @override
  void onInit() {
    // String x = Get.arguments;

    // TODO: implement onInit
    fetchCoureses();
  }

  void fetchCoureses() async {
    isLoading(true);
    try {
      var courses = await ApiService.fetch_courses();

      if (courses != null) {
        coursesList.value = courses;
      }
    } finally {
      isLoading(false);
    }
  }
}
