import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:trading/api/notes_services.dart';
import 'package:trading/models/notes_model.dart';

class NotesController extends GetxController {
  var isLoading = true.obs;
  var notesList = <Notes>[].obs;

  @override
  void onInit() {
    fetchnotes();
  }

  void fetchnotes() async {
    isLoading(true);
    try {
      var notes = await NotesService.fetch_notes();
      if (notes != null) {
        
        notesList.value = notes;
        
      }
    } finally {
      isLoading(false);
    }
  }

  void addNote(Notes note){

    notesList.add(note);
    NotesService.addnote(note.note, note.title);

  }

  void deleteNote(index,id){
    
// notesList.removeWhere((item) => item.id == id);
notesList.removeAt(index);
NotesService.removenotes(id: id);

  }
}
