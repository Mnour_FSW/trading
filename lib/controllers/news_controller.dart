import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/models/news_model.dart';
import 'package:get/get.dart';

class NewsController extends GetxController {
  var isLoading = true.obs;
  var newsList = <News>[].obs;

  // NewsController(arguments);
  @override
  void onInit() {
    // String x = Get.arguments;

    // TODO: implement onInit
    fetchnews();
  }

  void fetchnews() async {
    isLoading(true);
    try {
      var newss = await ApiService.fetch_news();

      if (newss != null) {
        newsList.value = newss;
      }
    } finally {
      isLoading(false);
    }
  }
}
