import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:trading/api/subsecrip_services.dart';

class SubsecriptionController extends GetxController {
  var isLoading = false.obs;
  doSubsecrip(mfResponse) async {
    isLoading(true);
    try {
      var data = await SubsecripServices.subsecrip(mfresponse: mfResponse);

      if (data != null) {
        await storage.write(key: "isFree", value: '0');
      }
    } finally {
      isLoading(false);
    }
  }
}
