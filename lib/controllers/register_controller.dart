import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/api/auth_services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:stream_chat_flutter/stream_chat_flutter.dart';
import 'package:trading/stream_chat/message_config.dart';

class RegisterController extends GetxController {
  var isLoading = false.obs;

  var isHidden = true.obs;
  var isConfirmHidden = true.obs;

  final registerFormKey = GlobalKey<FormState>();
  late String firbasetoken;
  late TextEditingController nameController,
      nickNameController,
      phoneController,
      emailController,
      confirmPasswordController,
      passwordController;
  String name = "",
      nickName = '',
      email = "",
      phone = "",
      password = "",
      confirmPassword = "",
      ftoken = "";
  final storage = const FlutterSecureStorage();

  @override
  void onInit() {
    nameController = TextEditingController();
    nickNameController = TextEditingController();

    phoneController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    confirmPasswordController = TextEditingController();
    super.onInit();
  }

  @override
  void dispose() {
    nameController.dispose();
    nickNameController.dispose();
    nickNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  togglePasswordView() {
    if (isHidden == true) {
      isHidden = false.obs;
    } else {
      isHidden = true.obs;
    }
  }

  String? validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return null;
      return "This is wrong Email";
    } else {
      return null;
    }
  }

  String? validatePassword(String value) {
    if (value.length <= 8) {
      return null;
      return "This is wrong Password";
    } else {
      return null;
    }
  }

  doRegister() async {
    bool isValidate = registerFormKey.currentState!.validate();
    if (isValidate) {
      isLoading(true);
      try {
        var data = await AuthServices.register(
            name: nameController.text,
            nickName: nickNameController.text,
            email: emailController.text,
            phone: phoneController.text,
            password: passwordController.text,
            confirmPassword: confirmPasswordController.text,
            ftoken: firbasetoken);
        if (data != null) {
          await storage.write(key: "id", value: data.user.id.toString());
          await storage.write(key: "name", value: data.user.name.toString());
          await storage.write(key: "phone", value: data.user.phone.toString());
          await storage.write(key: "email", value: data.user.email.toString());
          await storage.write(key: "token", value: data.user.ftoken.toString());
          await storage.write(
              key: "stream_token", value: data.user.stream_token.toString());
          await storage.write(
              key: "nickName", value: data.user.nick_name.toString());
          await storage.write(
              key: "isFree", value: data.user.isFree.toString());
          await storage.write(
              key: "active", value: data.user.ftoken.toString());

          await storage.write(key: "type", value: data.user.type.toString());

          await storage.write(key: "image", value: data.user.image.toString());
          registerFormKey.currentState!.save();

          final client = StreamChatClient(streamKey, logLevel: Level.INFO);

          await client.connectUser(
              User(
                  id: data.user.id.toString(),
                  name: data.user.nick_name.toString()),
              data.user.stream_token.toString());
          client.addDevice(data.user.ftoken.toString(), PushProvider.firebase);

          await client.disconnectUser();
          await client.connectUser(User(id: 'algorexe'),
              'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYWxnb3JleGUifQ.krQ5AeN-fcPq0WA9IvRqc77aLvb_nLK-s5kAnBTVQFU');

          final channelsm = client.channel('messaging', id: 'public_gorup');
          await channelsm.watch();
          await channelsm.addMembers([data.user.id.toString()]);

          final channelsmm = client.channel('messaging', id: 'support');
          await channelsmm.watch();
          await channelsmm.addMembers([data.user.id.toString()]);

          await client.disconnectUser();

          // Get.offAllNamed('myf');
          Get.offAllNamed('otp?phone=${phoneController.text}');
        } else {
          // Get.snackbar('register', data.toString());
        }
      } finally {
        isLoading(false);
      }
    }
  }
}
