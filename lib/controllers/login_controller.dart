import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/api/auth_services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginController extends GetxController {
  var isLoading = false.obs;
  var isHidden = true.obs;
  var loginFormKey = GlobalKey<FormState>();
  late TextEditingController emailController, passwordController;
  String email = "", password = "";
  final storage = const FlutterSecureStorage();

  @override
  void onInit() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.onInit();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  String? validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return "البريد الإلكتروني خطأ";
    } else {
      return null;
    }
  }

  String? validatePassword(String value) {
    if (value.length <= 3) {
      return "كلمة المرور خطأ";
    } else {
      return null;
    }
  }

  doLogin() async {
    bool isValidate = loginFormKey.currentState!.validate();
    if (isValidate) {
      isLoading(true);
      String? nameUser = await storage.read(key: "name");
      print('user name 1 *****' + nameUser.toString());
      try {
        var data = await AuthServices.login(
            email: emailController.text, password: passwordController.text);
        //print('data *****' + data.toString());

        if (data != null) {
          await storage.write(key: "id", value: data.user.id.toString());
          await storage.write(key: "name", value: data.user.name.toString());
          await storage.write(key: "email", value: data.user.email.toString());
          await storage.write(key: "phone", value: data.user.phone.toString());
          // await storage.write(
          //    key: "nickName", value: data.user.nickName.toString());
          await storage.write(
              key: "isFree", value: data.user.isFree.toString());
          await storage.write(key: "token", value: data.user.ftoken.toString());
          await storage.write(
              key: "active", value: data.user.active.toString());
          await storage.write(
              key: "phone_verified",
              value: data.user.phone_verified.toString());

          await storage.write(key: "type", value: data.user.type.toString());

          await storage.write(
              key: "ftoken", value: data.user.ftoken.toString());
          String? nameUser = await storage.read(key: "name");
          String? type = await storage.read(key: "active");
          String? active = await storage.read(key: "type");
          String? phone_verified = await storage.read(key: "phone_verified");

          loginFormKey.currentState!.save();

          if (phone_verified == '1') {
            Get.offAllNamed('home');
          } else {
            Get.offAllNamed('otp');
          }
        } else {
          print(data);
          Map<String, dynamic>? res = jsonDecode(data.toString());
          // print('Howdy, ${res['success']}');
          // Get.snackbar('login', "خطأ في البريد الإلكتروني أو كلمة السر $res");
        }
      } finally {
        isLoading(false);
      }
    }
  }
}
