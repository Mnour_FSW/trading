import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/models/market_model.dart';

class MarketController extends GetxController {
  var isLoading = true.obs;
  var marketList = <Market>[].obs;
  var currentPage = 0.obs;

  @override
  void onInit() {
    // TODO: implement onInit

    fetchMarket();
  }

  void fetchMarket() async {
    isLoading(true);
    try {
      var markets = await ApiService.fetch_markets();

      if (markets != null) {
        marketList.value = markets;
      }
    } finally {
      isLoading(false);
    }
  }
}
