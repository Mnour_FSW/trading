import 'package:get/state_manager.dart';
import 'package:trading/api/invoices_services.dart';
import 'package:get/get.dart';
import 'package:trading/models/invoices_model.dart';

class InvoicesController extends GetxController {
  var isLoading = true.obs;
  var invoicesList = <Invoices>[].obs;

  @override
  void onInit() {
    fetchinvoives();
  }

  void fetchinvoives() async {
    isLoading(true);
    try {
      var invoices = await InvoicesService.fetch_invoices();

      if (invoices != null) {
        invoicesList.value = invoices;
      }
    } finally {
      isLoading(false);
    }
  }
}
