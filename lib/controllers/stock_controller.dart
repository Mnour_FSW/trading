import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/controllers/favorite_controller.dart';
import 'package:trading/models/stock_model.dart';
import 'package:get/get.dart';

class StockController extends GetxController {
  var isLoading = true.obs;
  var stockList = <Stock>[].obs;

  // StockController(arguments);
  @override
  void onInit() {
    // String? x = Get.arguments;

    // // TODO: implement onInit
    // fetchstock(x);
  }

  void fetchstock(arguments) async {
    isLoading(true);
    try {
      var stocks = await ApiService.fetch_stocks(arguments);

      if (stocks != null) {
        stockList.value = stocks;
      }
    } finally {
      isLoading(false);
    }
  }

  void changeviews(index, id) {
    ApiService.postview(id: id);
    Stock stock;
    print(stockList[index].views);

    stock = stockList[index];

    stock.views = 0;

    stockList[index] = stock;

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void addtofavorite(code, id) {
    Stock stock;
    FavoriteController favoriteController = Get.put(FavoriteController());
    ApiService.addtofavorite(id: id);

    print(code);

    stock = stockList[stockList.indexWhere((element) => element.code == code)];

    stock.favorite = 1;

    stockList[stockList.indexWhere((element) => element.code == code)] = stock;
    favoriteController.favoriteList.add(stock);

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void removefromfavorite(code, id) {
    Stock stock;
    ApiService.removefavorite(id: id);

    stock = stockList[stockList.indexWhere((element) => element.code == code)];

    stock.favorite = 0;

    stockList[stockList.indexWhere((element) => element.code == code)] = stock;

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void changeViews(code) {
    Stock stock;

    var contain = stockList.where((element) => element.code == code);
    if (contain.isEmpty) {
      print('isImpty');
    } else {
      print('NotImpty');

      stock =
          stockList[stockList.indexWhere((element) => element.code == code)];

      stock.views = stock.views + 1;

      stockList[stockList.indexWhere((element) => element.code == code)] =
          stock;
    }
  }
}
