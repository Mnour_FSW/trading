import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/controllers/favorite_controller.dart';
import 'package:trading/models/stock_model.dart';
import 'package:get/get.dart';

class AllStockController extends GetxController {
  var isLoading = true.obs;
  var allstockList = <Stock>[].obs;

  @override
  void onInit() {
    fetchallstocks();
  }

  void fetchallstocks() async {
    isLoading(true);
    try {
      var allstocks = await ApiService.fetch_all_stocks();

      if (allstocks != null) {
        allstockList.value = allstocks;
      }
    } finally {
      isLoading(false);
    }
  }

  void changeviews(index, id) {
    ApiService.postview(id: id);
    Stock stock;
    print(allstockList[index].views);

    stock = allstockList[index];

    stock.views = 0;

    allstockList[index] = stock;

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void addtofavorite(code, id) {
    Stock stock;
    FavoriteController favoriteController = Get.put(FavoriteController());
    ApiService.addtofavorite(id: id);

    print(code);

    stock = allstockList[
        allstockList.indexWhere((element) => element.code == code)];

    stock.favorite = 1;

    allstockList[allstockList.indexWhere((element) => element.code == code)] =
        stock;
    favoriteController.favoriteList.add(stock);

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void removefromfavorite(code, id) {
    Stock stock;
    ApiService.removefavorite(id: id);

    stock = allstockList[
        allstockList.indexWhere((element) => element.code == code)];

    stock.favorite = 0;

    allstockList[allstockList.indexWhere((element) => element.code == code)] =
        stock;

    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();
  }

  void changeViews(code) {
    Stock stock;

    var contain = allstockList.where((element) => element.code == code);
    if (contain.isEmpty) {
      print('isImpty');
    } else {
      print('NotImpty');

      stock = allstockList[
          allstockList.indexWhere((element) => element.code == code)];

      stock.views = stock.views + 1;

      allstockList[allstockList.indexWhere((element) => element.code == code)] =
          stock;
    }
  }
}
