import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/models/reco_model.dart';
import 'package:trading/models/stock_model.dart';
import 'package:get/get.dart';

class RecoController extends GetxController {
  var isLoading = true.obs;
  var isEmpties = false;
  var recokList = <Reco>[].obs;
  var allrecokList = <Reco>[].obs;

  @override
  void onInit() {}

  void fetchreco(arguments) async {
    isLoading(true);
    try {
      var recos = await ApiService.fetch_recos(arguments);

      if (recos != null) {
        recokList.value = recos;
        print(recos);
        isLoading(false);
      } else {
        isEmpties = true;
        // Get.snackbar('title', 'message');
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchallreco(arguments) async {
    isLoading(true);
    try {
      var recos = await ApiService.fetch_all_recos(arguments);

      if (recos != null) {
        allrecokList.value = recos;
        isLoading(false);
      }
    } finally {
      isLoading(false);
    }
  }
}
