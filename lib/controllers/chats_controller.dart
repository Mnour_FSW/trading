import 'package:get/state_manager.dart';
import 'package:trading/api/chat_services.dart';
import 'package:trading/models/chat_model.dart';
import 'package:get/get.dart';

class ChatsController extends GetxController {
  var isLoading = true.obs;
  var chatsList = <Chats>[].obs;

  @override
  void onInit() {
    fetchchats();
  }

  void fetchchats() async {
    isLoading(true);
    try {
      var chats = await ChatService.fetch_chats();

      if (chats != null) {
        chatsList.value = chats;
      }
    } finally {
      isLoading(false);
    }
  }
}
