import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/state_manager.dart';
import 'package:trading/models/colors.dart';

class ColorsController extends GetxController {
  Color? selectColor;

  @override
  void onInit() {
    // TODO: implement onInit
    var sColor = selectColor.obs;
  }

  Color? _userColor;
  Future<void> getColor() async {
    final storage = FlutterSecureStorage();
    final String? userColor = await storage.read(key: "color");
    print("userColor" + userColor.toString());
  }
}
