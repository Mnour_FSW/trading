import 'package:get/state_manager.dart';
import 'package:trading/api/api_services.dart';
import 'package:trading/models/stock_model.dart';
import 'package:get/get.dart';

class FavoriteController extends GetxController {
  var isLoading = true.obs;
  var favoriteList = <Stock>[].obs;

  @override
  void onInit() {
    fetch_all_favorite();
  }

  void fetch_all_favorite() async {
    isLoading(true);
    try {
      var favorite = await ApiService.fetch_all_favorites();

      if (favorite != null) {
        favoriteList.value = favorite;
      }
    } finally {
      isLoading(false);
    }
  }


    void removefromfavorite(code,id){

        ApiService.removefavorite(id: id);

   


     

favoriteList.removeAt(favoriteList.indexWhere((element) => element.code == code));


    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();

  }


   void changeviews(index,id){
      ApiService.postview(id:id);
      Stock stock;
      print(favoriteList[index].views);
      
   
    stock = favoriteList[index];

      stock.views = 0;

      favoriteList[index] = stock;


    // fe.views = 0;
    // print(fe.views.toString());
    // stockList.refresh();
    update();

  }

  void changeViews(code){
Stock stock;

var contain = favoriteList.where((element) => element.code == code);
if (contain.isEmpty){

  print('isImpty');
}else{
  print('NotImpty');


   stock =   favoriteList[favoriteList.indexWhere((element) => element.code == code)];


      stock.views = stock.views+1;
      

       favoriteList[favoriteList.indexWhere((element) => element.code == code)] = stock;

}


  }
}
