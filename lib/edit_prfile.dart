import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trading/api/api_services.dart';

import 'models/colors.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  String? _name;
  String? _image;
  String? _nickName;
  String? _email;
  String? _phone;
  TextEditingController? _controller;
  PickedFile? _imagefile;
  final ImagePicker _picker = ImagePicker();

  getPhone() async {
    final storage = FlutterSecureStorage();
    final String? name = await storage.read(key: "name");
    final String? nickName = await storage.read(key: "nickName");
    final String? email = await storage.read(key: "email");
    final String? phone = await storage.read(key: "phone");
    final String? image = await storage.read(key: "image");
    print(name);

    setState(() {
      _name = name!;
      _image = image!;
      _nickName = nickName!;
      _email = email!;
      _phone = phone!;
    });
  }

  // String? name,age,genderPref,email;
  // void getBalance() async {
  //   name = await getPrefString(name_pref);
  //   age = await getPrefString(age_pref);
  //   genderPref = await getPrefString(gender_pref);
  //   email = await getPrefString(email_pref);
  //   print("name $name");
  //   print("email $email");
  //   setState(() {
  //     name;
  //     age;
  //     genderPref;
  //     email;
  //   });
  // }
  @override
  void initState() {
    //  getBalance();
    super.initState();
    getPhone();

    _controller = TextEditingController(text: _name);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffF5F5F5),
      appBar: AppBar(
        // toolbarHeight: 80,
        backgroundColor: context.theme.primaryColor,
        // elevation: 0,
        leadingWidth: 80,
        title: const Text(
          'تعديل الملف الشخصي',
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Get.offAllNamed('home');
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Stack(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width / 3,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 20,
                            ),
                          ],
                          shape: BoxShape.rectangle,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(100.0),
                          ),
                          image: _imagefile != null
                              ? DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(File(_imagefile!.path)))
                              : DecorationImage(
                                  fit: BoxFit.cover,
                                  image: _image != null
                                      ? CachedNetworkImageProvider(
                                          'http://www.trading.algorexe.com/images/${_image!}')
                                      : CachedNetworkImageProvider(
                                          'http://www.trading.algorexe.com/images/Users/user.png'),
                                ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (ctx) => AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(22),
                                      ),
                                      content: Directionality(
                                        textDirection: TextDirection.rtl,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Center(
                                              child: Text(
                                                'تغير الصورة الشخصية',
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Container(
                                              // width: 100,
                                              child: FlatButton(
                                                  splashColor: Colors.white,
                                                  color: Colors.grey.shade50,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  onPressed: () {
                                                    pickImage(
                                                        ImageSource.gallery);
                                                  },
                                                  child: Row(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Icon(
                                                          Icons.photo,
                                                          color: Colors.grey,
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Text(
                                                          'اختيار من الاستديو',
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      ])),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Container(
                                              child: FlatButton(
                                                  splashColor: Colors.white,
                                                  color: Colors.grey.shade50,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  onPressed: () async {
                                                    await pickImage(
                                                        ImageSource.camera);
                                                  },
                                                  child: Row(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Icon(
                                                          Icons
                                                              .camera_alt_rounded,
                                                          color: Colors.grey,
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Text(
                                                          'التقاط صورة من الكاميرا',
                                                          style: TextStyle(
                                                              fontSize: 11,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      ])),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 20,
                                ),
                              ],
                              shape: BoxShape.rectangle,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(100.0),
                              ),
                            ),
                            child: const Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Icon(Icons.edit),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: Column(
                      children: [
                        Text(
                          "$_phone",
                          style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey[700],
                              fontWeight: FontWeight.bold),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "تغير رقم الهاتف",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.grey[500],
                                  fontWeight: FontWeight.bold),
                            ),
                            Icon(
                              Icons.edit,
                              color: Colors.grey[500],
                              size: 18,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Text(
                  'الاسم الثلاثي',
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                ),
                TextField(
                  maxLines: 1,
                  minLines: 1,
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    _name = value;
                  },
                  style: const TextStyle(
                      fontSize: 18.0,
                      // height: 1.3,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  decoration: InputDecoration(
                    hintText: '$_name',

                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 8), //  <- you can it to 0.0 for no space
                    isDense: true,
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.9))),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(
                  'الاسم المستعار',
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                ),
                TextField(
                  //maxLength: 10,
                  maxLines: 1,
                  minLines: 1,
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    _nickName = value;
                  },

                  style: const TextStyle(
                      fontSize: 18.0,
                      height: 1.3,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  decoration: InputDecoration(
                    hintText: _nickName,
                    //labelText: "Phone number",
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 8), //  <- you can it to 0.0 for no space
                    isDense: true,
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.9))),

                    //border: InputBorder.none
                  ),
                  // controller: getIt<ProfileProvider>().editName,
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(
                  'البريد الإلكتروني',
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                ),
                TextField(
                  controller: _controller,
                  //maxLength: 10,
                  maxLines: 1,
                  minLines: 1,
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (value) {
                    _email = value;
                  },
                  style: const TextStyle(
                      fontSize: 18.0,
                      height: 1.3,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  decoration: InputDecoration(
                    hintText: _email,
                    //labelText: "Phone number",

                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 8), //  <- you can it to 0.0 for no space
                    isDense: true,
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey.withOpacity(0.9),
                      ),
                    ),

                    //border: InputBorder.none
                  ),
                  // controller: getIt<ProfileProvider>().editEmail,
                ),

                const SizedBox(
                  height: 20,
                ),
                // Container(
                //   child: Column(
                //     children: [
                //       Row(
                //         children: [
                //           Icon(
                //             Icons.date_range_outlined,
                //             color: Colors.grey[600],
                //           ),
                //           SizedBox(
                //             width: 5,
                //           ),
                //           Text(
                //             'كبي',
                //             style:
                //                 TextStyle(fontSize: 16, color: Colors.grey[700]),
                //           )
                //         ],
                //       ),
                //       TextField(
                //         //maxLength: 10,
                //         maxLines: 1,
                //         minLines: 1,
                //         keyboardType: TextInputType.number,
                //         style: TextStyle(
                //             fontSize: 14.0, height: 1.3, color: Colors.black),
                //         decoration: InputDecoration(

                //           contentPadding: EdgeInsets.all(5), //  <- you can it to 0.0 for no space
                //           isDense: true,
                //           enabledBorder: UnderlineInputBorder(
                //             borderSide: BorderSide(
                //               color: Colors.grey.withOpacity(0.4),
                //             ),
                //           ),

                //           //border: InputBorder.none
                //         ),
                //         // controller: getIt<ProfileProvider>().editAge,
                //       ),
                //     ],
                //   ),
                // ),

                SizedBox(
                  height: 30,
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: InkWell(
                    onTap: () {
                      ApiService.editProfile(_name, _nickName, _email);
                      Get.snackbar('تم', 'تم تعديل الملف الشخصي');

                      Future.delayed(const Duration(milliseconds: 1000), () {
// Here you can write your code
                        Get.toNamed('home');

                        setState(() {
                          // Here you can write your code for open new view
                        });
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 50,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          width: MediaQuery.of(context).size.width - 80,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            gradient: const LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [
                                Color(0xff72D49A),
                                Color(0xff3C8B62),
                              ],
                            ),
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                          ),
                          child: const Center(
                            child: Text(
                              'حفظ',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future pickImage(ImageSource source) async {
    try {
      PickedFile? image = await ImagePicker().getImage(source: source);
      setState(() {
        _imagefile = image;
        // getIt<ProfileProvider>().profileImage = File(image.path);
        // print("failed to pick image: $image");
        // print("failed to pick image: ${getIt<ProfileProvider>().profileImage}");
      });
      ApiService.editImageProfile(_imagefile);
      Navigator.pop(context);
      // ignore: nullable_type_in_catch_clause
    } on PlatformException catch (e) {
      print("failed to pick image: $e");
    }
  }
}
