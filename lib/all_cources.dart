import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/courses_controller.dart';
import 'package:trading/widgets/cources_videos_widget.dart';

class AllCourses extends StatelessWidget {
  const AllCourses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final CoursesController coursesController = Get.put(CoursesController());
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          backgroundColor: Colors.grey[200],
          appBar: AppBar(
            backgroundColor: context.theme.primaryColor,
            title: const Text('الدروس التعليمية'),
          ),
          body: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Expanded(
                  child: Obx(() {
                    if (coursesController.isLoading.value) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return ListView.builder(
                          padding: const EdgeInsets.all(0.0),
                          itemCount: coursesController.coursesList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return CourcesVideo(
                              url: coursesController.coursesList[index].link,
                              title: coursesController.coursesList[index].title,
                            );
                          });
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }
}
