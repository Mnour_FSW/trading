import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trading/controllers/all_stock_controller.dart';
import 'package:trading/widgets/allsouck_widget.dart';
import 'package:trading/widgets/souk_widgets.dart';

late final AllStockController stockController = Get.put(AllStockController());

class AllStocks extends StatelessWidget {
  final AllStockController stockController = Get.put(AllStockController());
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: const Color(0xffF5F5F5),
        appBar: AppBar(
            // toolbarHeight: 80,
            backgroundColor: context.theme.primaryColor,
            elevation: 0,
            title: const Text(
              "كل الأسواق",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            leadingWidth: 10,
            leading: Center()),
        body: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: Obx(() {
                if (stockController.isLoading.value) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return ListView.builder(
                      itemCount: stockController.allstockList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            stockController.changeviews(
                                index, stockController.allstockList[index].id);
                            Get.toNamed(
                              'sahim?id=${stockController.allstockList[index].id}&code=${stockController.allstockList[index].code}&name=${stockController.allstockList[index].name}&image=${'http://www.trading.algorexe.com/images/${stockController.allstockList[index].image}'}',
                            );
                          },
                          child: AllSoukWidgetsCard(
                            isfavorite: 0,
                            id: stockController.allstockList[index].id,
                            views: stockController.allstockList[index].views,
                            code: stockController.allstockList[index].code,
                            name: stockController.allstockList[index].name,
                            favorite:
                                stockController.allstockList[index].favorite,
                            image:
                                'http://www.trading.algorexe.com/images/${stockController.allstockList[index].image}',
                          ),
                        );
                      });
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}
