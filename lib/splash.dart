import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

// final ColorsController colorController = Get.put(ColorsController());

class _SplashScreenState extends State<SplashScreen> {
  void initState() {
    super.initState();

    initApp();
  }

  initApp() async {
    const storage = FlutterSecureStorage();
    String? nameUser = await storage.read(key: "name");

    String? phoneVerified = await storage.read(key: 'phone_verified');
    String? isFree = await storage.read(key: 'isFree');
    // Color? colorApp = hexToColor(ccc!);
    // setState(() => appColor = colorApp);
    // print('colorApp = $colorApp');
    print('user name= ' +
        nameUser.toString() +
        '\nphoneVerified=' +
        phoneVerified.toString() +
        '\nisFree=' +
        isFree.toString());
    Future.delayed(Duration(seconds: 3), () {
      if (nameUser != null && phoneVerified == '1') {
        Get.offAllNamed('home');
      } else if (nameUser == null && phoneVerified == null) {
        Get.toNamed('login');
      } else {
        Get.toNamed('otp');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // Color? color = DynamicColorTheme.of(context).color;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 250,
                              width: 250,
                              decoration: const BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage('assets/images/mec.png'),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              'MARKET EXPERTS CLUB ',
                              style: TextStyle(
                                  color: Colors.grey[700],
                                  // color: ,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xff71c93e)),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    const Text(
                      "Loading\nExperience",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton.extended(
      //   backgroundColor: color,
      //   icon: Icon(Icons.color_lens),
      //   label: Text('Color Picker'),
      //   onPressed: () {
      //     showDialog(
      //       builder: (BuildContext context) {
      //         return WillPopScope(
      //           child: ColorPickerDialog(
      //             defaultColor: kFuchsia,
      //             defaultIsDark: false,
      //             title: 'Choose your Destiny',
      //             cancelButtonText: 'NEVERMIND',
      //             confirmButtonText: 'SOUNDS GOOD',
      //             shouldAutoDetermineDarkMode: true,
      //             shouldShowLabel: true,
      //           ),
      //           onWillPop: () async {
      //             DynamicColorTheme.of(context).resetToSharedPrefsValues();
      //             return true;
      //           },
      //         );
      //       },
      //       context: context,
      //     );
      //   },
      // ),
    );
  }
}
