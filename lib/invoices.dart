import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/invoices_controller.dart';
import 'widgets/invoices_widget.dart';

// final invoicesController = Get.find<InvoicesController>();

class InvoicesScreen extends StatelessWidget {
  final InvoicesController invoicesController = Get.put(InvoicesController());
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            title: const Text("ايصالات الدفع"),
            backgroundColor: context.theme.primaryColor,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Get.lazyPut(() => InvoicesController());
                Navigator.pop(context);
              },
            ),
          ),
          body: Column(
            children: [
              Expanded(
                child: Obx(() {
                  if (invoicesController.isLoading.value) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListView.builder(
                        padding: EdgeInsets.all(0.0),
                        // physics: const NeverScrollableScrollPhysics(),
                        itemCount: invoicesController.invoicesList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InvoiceWidget(
                            invocesId:
                                invoicesController.invoicesList[index].id,
                            status:
                                invoicesController.invoicesList[index].status,
                            amount:
                                invoicesController.invoicesList[index].amount,
                            createdAt: invoicesController
                                .invoicesList[index].createdAt,
                          );
                        });
                  }
                }),
              ),
            ],
          )),
    );
  }
}
