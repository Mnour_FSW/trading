import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/stock_controller.dart';

class SahimScreen extends StatefulWidget {
  @override
  _SoukScreenState createState() => _SoukScreenState();
}

// late final ViewController viewController =
//     Get.put(ViewController(Get.parameters['id']));

final StockController stockController = Get.put(StockController());

class _SoukScreenState extends State<SahimScreen>
    with SingleTickerProviderStateMixin {
  late Query _ref;
  bool check = false;

  // void checkEmptyList() async {
  //   String? isEmpties;
  //   var data =
  //       await _ref.reference().child(Get.parameters['code'].toString()).once();
  //   if (data.value == null) {
  //     isEmpties = 'true';
  //     print('isEmpties' + isEmpties.toString());
  //   } else {
  //     isEmpties = 'false';
  //     print('isEmpties' + isEmpties.toString());
  //   }
  // }

  @override
  void initState() {
    super.initState();

    _ref = FirebaseDatabase.instance
        .reference()
        .child(Get.parameters['code'].toString());

    _ref.once().then((DataSnapshot data) {
      if (data.value == null) {
        setState(() {
          check = true;
        });

        print(check);
      } else {
        print(check);
      }
    });

    // print('hello object' + _ref.isBlank);
    // if (_ref.toString() == null) {
    //   print('hello object');
    // } else {
    //   print('hello fuck');
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: NestedScrollView(
          physics: const BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                // actions: <Widget>[
                //   IconButton(
                //       icon: Transform(
                //         alignment: Alignment.center,
                //         transform: Matrix4.rotationY(math.pi),
                //         child: Icon(Icons.reply),
                //       ),
                //       onPressed: () {}),
                //   IconButton(icon: Icon(Icons.more_horiz), onPressed: () {}),
                // ],
                leading: IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                backgroundColor: context.theme.primaryColor,
                expandedHeight: 170.0,
                // title: InkWell(
                //   onTap: () {
                //     Get.toNamed('search');
                //   },
                //   child: Container(
                //     height: 40,
                //     padding:
                //         EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                //     decoration: BoxDecoration(
                //       color: Colors.grey[100],
                //       borderRadius: BorderRadius.circular(10),
                //     ),
                //     child: TextFormField(
                //       readOnly: true,
                //       onTap: () {
                //         //showSearch(context: context, delegate: DataSearch());
                //       },
                //       decoration: InputDecoration(
                //         suffixIcon: IconButton(
                //           onPressed: () {},
                //           icon: Icon(Icons.search,
                //               size: 25, color: Colors.grey),
                //         ),
                //         contentPadding: EdgeInsets.symmetric(vertical: 2),
                //         hintText: "بحث",
                //         hintStyle:
                //             TextStyle(fontSize: 18.0, color: Colors.grey),
                //         border: InputBorder.none,
                //       ),
                //     ),
                //   ),
                // ),
                // pinned: true,
                snap: false,
                pinned: true,
                titleSpacing: 2,
                stretch: true,
                floating: false,
                // **Is it intended ?** flexibleSpace.title overlaps with tabs title.
                flexibleSpace: FlexibleSpaceBar(
                  stretchModes: [
                    StretchMode.zoomBackground,
                    StretchMode.blurBackground,
                    StretchMode.fadeTitle
                  ],
                  background: Stack(children: [
                    Container(
                      height: 210,
                      color: Colors.white,
                      width: double.infinity,
                    ),
                    Container(
                      height: 210,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              context.theme.primaryColorDark,
                              context.theme.primaryColor,
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                    ),
                    Positioned(
                      top: 115,
                      left: 25,
                      right: 25,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 50.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    image: DecorationImage(
                                      image: CachedNetworkImageProvider(
                                          Get.parameters['image'].toString()),
                                      fit: BoxFit.cover,
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.3),
                                        spreadRadius: 2,
                                        blurRadius: 5,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(8.0)),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  height: 75,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 5,
                                      left: 8.0,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.6,
                                          child: Text(
                                            Get.parameters['code'].toString(),
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          Get.parameters['name'].toString(),
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ];
          },
          body: check == false
              ? SizedBox(
                  height: double.infinity,
                  child: FirebaseAnimatedList(
                    sort: (a, b) => b.key!.compareTo(a.key!),
                    reverse: false,
                    defaultChild: Center(
                      child: CircularProgressIndicator(),
                    ),
                    itemBuilder: (BuildContext context, DataSnapshot snapshot,
                        Animation<double> animation, int index) {
                      return Container(
                          width: MediaQuery.of(context).size.width - 40,
                          margin: const EdgeInsets.all(20),
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.4),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: const Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Row(
                              //   children: [
                              //     Container(
                              //       height: 40,
                              //       width: 40,
                              //       decoration: BoxDecoration(
                              //         color: Colors.white,
                              //         image: DecorationImage(
                              //           image: NetworkImage(
                              //               'https://www.dfm.ae/images/default-source/default-album/aan-logo-01.png?sfvrsn=57bebd8e_0'),
                              //           fit: BoxFit.contain,
                              //         ),
                              //         border: Border.all(color: Colors.grey),
                              //         borderRadius:
                              //             BorderRadius.all(Radius.circular(8.0)),
                              //       ),
                              //     ),
                              //     SizedBox(
                              //       width: 10,
                              //     ),
                              //     Container(
                              //       //color: Colors.red,
                              //       //width: MediaQuery.of(context).size.width / 2.5,

                              //       child: Padding(
                              //         padding: const EdgeInsets.only(
                              //           top: 5,
                              //           left: 8.0,
                              //         ),
                              //         child: Column(
                              //           crossAxisAlignment: CrossAxisAlignment.start,
                              //           children: [
                              //             Container(
                              //               width:
                              //                   MediaQuery.of(context).size.width / 2,
                              //               child: Text(
                              //                 "ANN " +
                              //                     "شركة آن ديجيتال سيرفسس القابضة",
                              //                 overflow: TextOverflow.ellipsis,
                              //                 style: TextStyle(
                              //                   color: Colors.black,
                              //                   fontWeight: FontWeight.w700,
                              //                   fontSize: 20,
                              //                 ),
                              //               ),
                              //             ),
                              //           ],
                              //         ),
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width - 80,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      '${snapshot.value["created_at"]}',
                                      style: TextStyle(color: Colors.grey[700]),
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              snapshot.value["image"] != null
                                  ? GestureDetector(
                                      onTap: () {
                                        Get.toNamed(
                                            'sahim_image?image=${snapshot.value["image"]}');
                                      },
                                      child: Container(
                                        height:
                                            MediaQuery.of(context).size.width /
                                                2,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          color: Colors.grey,
                                          image: DecorationImage(
                                            image: NetworkImage(
                                                'http://www.trading.algorexe.com/images/${snapshot.value["image"]}'),
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(8.0)),
                                        ),
                                      ),
                                    )
                                  : Container(),

                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.start,
                              //   children: [
                              //     SizedBox(
                              //       // padding: EdgeInsets.all(10),
                              //       width: MediaQuery.of(context).size.width - 80,
                              //       child: Text(
                              //         'الاستشارة :' + snapshot.value['title'],
                              //         style: const TextStyle(fontSize: 14),
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              Row(
                                children: [
                                  Container(
                                    // padding: EdgeInsets.all(10),
                                    width:
                                        MediaQuery.of(context).size.width - 80,
                                    child: Text(
                                      'الاستشارة :' + snapshot.value['title'],
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    'الدعوم :',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    snapshot.value['up'],
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  const Text(
                                    'المقاومات :',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    snapshot.value['down'],
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.green,
                                    ),
                                  ),
                                ],
                              ),
                              if (snapshot.value["image"] != null)
                                Row(
                                  children: const [
                                    Text(
                                      'الملاحظات :',
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ],
                                )
                              else
                                Container(),
                            ],
                          ));
                    },
                    query: _ref,
                  ))
              : Center(
                  child: Container(
                    height: 270,
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 100,
                          backgroundImage: AssetImage('assets/images/e.jpeg'),
                        ),
                        Text(
                          'لا يوجد استشارات جديدة',
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
