import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:myfatoorah_flutter/myfatoorah_flutter.dart';
import 'package:trading/controllers/subsecription_controller.dart';

class MyFatoora extends StatefulWidget {
  static const String id = "Packages";
  const MyFatoora({Key? key}) : super(key: key);

  @override
  State<MyFatoora> createState() => _MyFatoora();
}

class _MyFatoora extends State<MyFatoora> {
  @override
  // ignore: must_call_super
  final storage = FlutterSecureStorage();

  final SubsecriptionController c = Get.put(SubsecriptionController());
  // final SubsecriptionController c = Get.find();
  String _response = '';
  String _loading = "Loading...";
  bool loading = false;
  final String mAPIKey =
      "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";

  String? _userId;
  Future<void> getId() async {
    final storage = FlutterSecureStorage();
    final String? userId = await storage.read(key: "id");
    print(userId);

    setState(() => _userId = userId!);
  }

  String? _userToken;
  Future<void> getToken() async {
    final storage = FlutterSecureStorage();
    final String? userToken = await storage.read(key: "token");
    // print(userToken);

    setState(() => _userToken = userToken!);
  }

  String? _userName;
  Future<void> getName() async {
    final storage = FlutterSecureStorage();
    final String? userName = await storage.read(key: "name");
    print(userName);

    setState(() => _userName = userName!);
  }

  String? _userEmail;
  Future<void> getEmail() async {
    final storage = FlutterSecureStorage();
    final String? userEmail = await storage.read(key: "email");
    print(userEmail);

    setState(() => _userEmail = userEmail!);
  }

  @override
  void initState() {
    // print('body =>');
    getId();
    getName();
    getEmail();
    getToken();
    super.initState();
    if (mAPIKey.isEmpty) {
      setState(() {
        _response = "https://apitest.myfatoorah.com/";
      });
      return;
    }

    MFSDK.init('https://apitest.myfatoorah.com/', mAPIKey);

    // executeRegularPayment(10.00);
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Color(0xffF5F5F5),
        appBar: AppBar(
          // toolbarHeight: 80,
          backgroundColor: context.theme.primaryColor,
          elevation: 0,
          title: const Text(
            "تجديد الأشتراك",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          // centerTitle: true,
          leadingWidth: 70,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: const Icon(
              Icons.arrow_back_ios,
              size: 25,
              color: Colors.white,
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  // Text('$_userName' +
                  //     '$_userEmail'
                  //         '$_userToken'
                  //         '$_userId\n'),
                  InkWell(
                    onTap: () {
                      executeRegularPayment(1000);
                    },
                    child: const package_widget(
                      name: 'الباقة الفضية',
                      image:
                          'https://soolef-kw.com/simages/Silver-Package%20copy.png',
                      value: 'شهر واحد',
                      price: 'AED 1000',
                      link: '',
                      color1: Color(0xFFCDCDCD),
                      color2: Color(0xFFB4B4B4),
                      color3: Color(0xFF828282),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      executeRegularPayment(5400);
                      /*      setState(() {
                loading=true;
               if (getIt<PaymentProvider>().paymentLoading==false){
                  loading=false;
                }
              });*/
                    },
                    child: const package_widget(
                      name: 'الباقة الذهبية',
                      image:
                          'https://soolef-kw.com/simages/Gold-Package%20copy.png',
                      value: 'ستة أشهر',
                      price: '5400 AED',
                      link: '',
                      color1: Color(0xFFF0CC46),
                      color2: Color(0xFFE1B13F),
                      color3: Color(0xFFC79335),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      executeRegularPayment(9000);
                    },
                    child: const package_widget(
                      name: 'الباقة البلاتينية',
                      image: 'https://soolef-kw.com/simages/pla-Package.png',
                      value: 'سنة واحدة',
                      price: '9000 AED',
                      link: '',
                      color1: Color(0xFFA4B7BE),
                      color2: Color(0xFF7F9BA4),
                      color3: Color(0xFF4E6570),
                    ),
                  )
                ],
              ),
            ),
            loading == true
                ? Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          context.theme.primaryColor),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  void executeRegularPayment(double price) {
    int paymentMethod = 2;
    var request = MFExecutePaymentRequest(
      paymentMethod,
      price,
      MFCurrencyISO.UAE_AED,
      '$_userName',
      '$_userEmail',
    );
    setState(() {
      loading = true;
    });
    MFSDK.executePayment(
        context,
        request,
        MFAPILanguage.EN,
        (String invoiceId, MFResult<MFPaymentStatusResponse> result) => {
              // print("Response from Younes: " +
              //     result.response!.toJson().toString()),
              if (result.isSuccess())
                {
                  setState(() {
                    print("Response from Younes 1 : " +
                        json.encode(result.response!.toJson()));
                    c.doSubsecrip(json.encode(result.response!.toJson()));

                    loading = false;
                  })
                }
              else
                {
                  setState(() {
                    // print(result.status!);
                    // print(result.error!);
                    // print("invoiceId: " + invoiceId);
                    // print(result.response);
                    // c.doSubsecrip(
                    //     '$_userId', '$_userToken', result.error!.toJson());
                    // print(
                    //     "Response error: " + result.error!.toJson().toString());
                    // _response = result.error.message;
                    loading = false;
                  })
                }
            });
    MFSDK.setUpAppBar(
        title: "Market Payment",
        titleColor: Colors.white, // Color(0xFFFFFFFF)
        backgroundColor: context.theme.primaryColor, // Color(0xFF000000)

        isShowAppBar: true);

    setState(() {
      _response = _loading;
    });
  }
}

class package_widget extends StatelessWidget {
  final String name;
  final Color color1;
  final Color color2;
  final Color color3;
  final String value;
  final String price;
  final String image;
  final String link;
  const package_widget({
    required this.name,
    required this.color1,
    required this.color2,
    required this.color3,
    required this.value,
    required this.price,
    required this.image,
    required this.link,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      height: 200,
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          stops: const [0.1, 0.4, 0.7, 0.9],
          colors: [
            color2,
            color1,
            color2,
            color3,
          ],
        ),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 0),
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            width: 160,
            height: 160,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                image: CachedNetworkImageProvider(image),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  name,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  value,
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  ' السعر : $price',
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.only(left: 20),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white.withOpacity(0.8),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          'اشترك الأن',
                          style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Container(
                        width: 20,
                        child: Icon(Icons.arrow_forward_ios, size: 15),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
