import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/login_controller.dart';

class LoginScreen extends GetView<LoginController> {
  LoginController ccontroller = Get.find();
  bool invisible = true;
  // bool _isHidden = true;

  @override
  Widget build(BuildContext context) {
    final emailField = Container(
      height: 30,
      child: TextFormField(
        scrollPadding: EdgeInsets.all(0),
        controller: controller.emailController,
        obscureText: false,
        validator: (v) {
          return controller.validateEmail(v!);
        },
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.grey,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            suffixIcon: Icon(
              Icons.email_outlined,
              color: Colors.grey,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'البريد الإلكتروني',

            //
            labelStyle: TextStyle(
              color: Colors.grey,
            )),
      ),
    );

    final passwordField = Container(
      height: 35,
      child: Obx(
        () => TextFormField(
          obscureText: controller.isHidden.value,
          controller: controller.passwordController,
          validator: (v) {
            return controller.validatePassword(v!);
          },
          keyboardType: TextInputType.visiblePassword,
          style: TextStyle(
            color: Colors.grey,
            decorationColor: Colors.grey,
          ),
          cursorColor: Colors.black,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            suffixIcon: InkWell(
              onTap: () {
                controller.isHidden.value = !controller.isHidden.value;
              },
              child: Icon(
                controller.isHidden.value
                    ? Icons.visibility
                    : Icons.visibility_off,
                color: Colors.grey,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'تأكيد كلمة السر',
          ),
        ),
      ),
    );

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 10,
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height / 4,
                            width: MediaQuery.of(context).size.height / 4,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage('assets/images/mec.png'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 15,
                    ),
                    Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Color(0xff545454),
                          fontSize: 24,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 20,
                    ),
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.6,
                          child: Column(
                            children: [
                              Container(
                                child: Form(
                                  key: controller.loginFormKey,
                                  child: Column(
                                    children: [
                                      emailField,
                                      SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                20,
                                      ),
                                      passwordField,
                                      SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                15,
                                      ),
                                      Container(
                                        width: double.infinity,
                                        height: 45.0,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Color(0xff71c93e)),
                                        child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            onTap: () {
                                              controller.doLogin();
                                            },
                                            child: Center(
                                              child: Obx(
                                                () => controller
                                                            .isLoading.value ==
                                                        true
                                                    ? Center(
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              'تحميل',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 20,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                            const SizedBox(
                                                              width: 20,
                                                            ),
                                                            Container(
                                                              height: 20,
                                                              width: 20,
                                                              child:
                                                                  CircularProgressIndicator(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    : Text(
                                                        'تسجيل الدخول',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .grey[800],
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "هل نسيت كلمة السر؟",
                                style: TextStyle(
                                  color: Color(0xffA8C0D4),
                                  fontSize: 12,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      height: 1,
                                      width: 30,
                                      color: Colors.white),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "أو",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 18),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      height: 1,
                                      width: 30,
                                      color: Colors.white),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Get.offNamed('registration');
                                },
                                child: Text(
                                  "إنشاء حساب جديد",
                                  style: TextStyle(
                                      color: Color(0xff545454), fontSize: 18),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // Positioned(
            //   top: 0,
            //   bottom: 0,
            //   left: 0,
            //   right: 0,
            //   child: Container(
            //     color: Colors.white.withOpacity(0.85),
            //     //
            //   ),
            // )
          ],
        ),
      ),
    );

    eye() {
      print('ddf');
    }
  }
}

class Email extends StatelessWidget {
  const Email({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.white,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
            suffixIcon: Icon(
              Icons.email_outlined,
              color: Colors.grey,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
            ),
            hintText: 'someone@email.com',
            //
            labelStyle: TextStyle(
              color: Colors.white,
            )),
      ),
    );
  }
}

class Password extends StatelessWidget {
  const Password({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        style: TextStyle(
          color: Colors.grey,
          decorationColor: Colors.white,
        ),
        cursorColor: Colors.black,
        decoration: InputDecoration(
          suffixIcon: Icon(
            Icons.lock_outlined,
            color: Color(0xffA8C0D4),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffA8C0D4),
            ),
          ),
          border:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffA8C0D4),
            ),
          ),
          hintStyle: TextStyle(
            color: Color(0xffA8C0D4),
          ),
          hintText: 'Password',
          // labelText: 'Password',
          // labelStyle: TextStyle(
          //   color: Colors.white,
          // )
        ),
      ),
    );
  }
}

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;
  final double width;
  final double height;
  final Function onPressed;

  const RaisedGradientButton({
    required Key key,
    required this.child,
    required this.gradient,
    this.width = double.infinity,
    this.height = 45.0,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 45.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: gradient,
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            // onTap: onPressed,
            // child: Center(
            //   child: child,
            // ),
            ),
      ),
    );
  }
}
