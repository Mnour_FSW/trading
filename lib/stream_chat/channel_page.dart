
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:stream_chat_flutter/stream_chat_flutter.dart';
import 'package:trading/stream_chat/audio_player.dart';
import 'package:trading/stream_chat/chat_info_screen.dart';
import 'package:trading/stream_chat/group_info_screen.dart';
import 'package:trading/stream_chat/record_button.dart';

import 'audio_loading.dart';

class ChannelPage extends StatefulWidget {
  const ChannelPage({
    Key? key,
  }) : super(key: key);

  @override
  _ChannelPageState createState() => _ChannelPageState();
}

class _ChannelPageState extends State<ChannelPage> {

  void _recordingFinishedCallback(String path) {
  final uri = Uri.parse(path);
  File file = File(uri.path);
  file.length().then(
    (fileSize) {
      StreamChannel.of(context).channel.sendMessage(
            Message(
              attachments: [
                Attachment(
                  type: 'voicenote',
                  file: AttachmentFile(
                    size: fileSize,
                    path: uri.path,
                  ),
                )
              ],
            ),
          );
    },
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:ChannelHeader(
        showTypingIndicator: false,
        onImageTap: () async {
          var channel = StreamChannel.of(context).channel;

          if (channel.memberCount == 2 && channel.isDistinct) {
            final currentUser = StreamChat.of(context).currentUser;
            final otherUser = channel.state!.members.firstWhere(
              (element) => element.user!.id != currentUser!.id,
            );
            if (otherUser != null) {
              final pop = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => StreamChannel(
                    child: ChatInfoScreen(
                      messageTheme: StreamChatTheme.of(context).ownMessageTheme,
                      user: otherUser.user,
                    ),
                    channel: channel,
                  ),
                ),
              );

              if (pop == true) {
                Navigator.pop(context);
              }
            }
          } else {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => StreamChannel(
                  child: GroupInfoScreen(
                    messageTheme: StreamChatTheme.of(context).ownMessageTheme,
                  ),
                  channel: channel,
                ),
              ),
            );
          }
        },
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: MessageListView(
  messageBuilder: (context, details, messages, defaultMessage) {
    return defaultMessage.copyWith(
      customAttachmentBuilders: {
        'voicenote': (context, defaultMessage, attachments) {
          final url = attachments.first.assetUrl;
          if (url == null) {
            return const AudioLoadingMessage();
          }
          return AudioPlayerMessage(
            source: AudioSource.uri(Uri.parse(url)),
            id: defaultMessage.id,
          );
        }
      },
    );
  },
),
          ),
         MessageInput(
  actions: [
    RecordButton(
      recordingFinishedCallback: _recordingFinishedCallback,
    ),
  ],
),
        ],
      ),
    );
  }
}
