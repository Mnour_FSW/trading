import 'package:flutter/material.dart';
import 'package:stream_chat_flutter/stream_chat_flutter.dart';
import 'package:trading/stream_chat/channel_page.dart';

class ChannelListPage extends StatelessWidget {
  const ChannelListPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Stream Chat'),
        actions: [
          GestureDetector(
            onTap: () async {
              // await StreamChat.of(context).client.disconnectUser();
              // Navigator.of(context).pushReplacement(
              //   MaterialPageRoute(builder: (context) => const SelectUserPage()),
              // );
            },
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Text('Switch user'),
              ),
            ),
          )
        ],
      ),
      body: Directionality(
        
        textDirection: TextDirection.rtl,
        child: ChannelsBloc(
          child: ChannelListView(
            // emptyBuilder: (conext) {
            //   return Center(
            //     child: ElevatedButton(
            //       onPressed: () async {
            //         final channel = StreamChat.of(context).client.channel(
            //           "messaging",
            //           id: "test-gordon",
            //           extraData: {
            //             "name": "Flutter Chat",
            //             "image":
            //                 "https://flutter.dev/assets/images/shared/brand/flutter/logo/flutter-lockup.png",
                      
            //           },
            //         );
            //         await channel.create();
            //       },
            //       child: const Text('Create channel'),
            //     ),
            //   );
            // },
            filter:
                Filter.in_('members', [StreamChat.of(context).currentUser!.id]),
            sort: const [SortOption('last_message_at')],
            pagination: const PaginationParams(limit: 40),
            channelWidget: const ChannelPage(),
          ),
        ),
      ),
    );
  }
}