import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:trading/main.dart';

import 'models/colors.dart';

class ChangeColors extends StatefulWidget {
  const ChangeColors({Key? key}) : super(key: key);

  @override
  State<ChangeColors> createState() => _ChangeColorsState();
}

final storage = const FlutterSecureStorage();

class _ChangeColorsState extends State<ChangeColors> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('تعديل اللون'),
          backgroundColor: context.theme.primaryColor,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Get.toNamed('home');
            },
          ),
        ),
        body: GridView.builder(
          itemCount: Themes.change.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount:
                  (MediaQuery.of(context).orientation == Orientation.portrait)
                      ? 4
                      : 4),
          itemBuilder: (BuildContext context, int index) {
            print(index.toString());
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  Themes.changeThemeColor(index);

                  AdaptiveTheme.of(context)
                      .setTheme(light: Themes.change[index]);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Themes.change[index].primaryColor,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 2,
                      ),
                    ],
                  ),
                  height: MediaQuery.of(context).size.width / 4 - 20,
                  width: MediaQuery.of(context).size.width / 4 - 20,
                  // child: color == const Color(kPr)
                  //     ? const Center(
                  //         child: Icon(
                  //           Icons.check,
                  //           color: Colors.white,
                  //         ),
                  //       )
                  //     : const Center(),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class Themes {
  static final List<ThemeData> change = [
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KTealDark),
      primaryColor: const Color(KTeal1),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KTealDark),
      primaryColor: const Color(KTeal2),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KTealDark),
      primaryColor: const Color(KTeal3),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KTealDark),
      primaryColor: const Color(KTeal4),
    ),
    ThemeData.light().copyWith(
        backgroundColor: Colors.grey[200],
        primaryColor: const Color(kRedDark),
        primaryColorDark: const Color(kRed2)),
    ThemeData.light().copyWith(
        backgroundColor: Colors.grey[200],
        primaryColor: const Color(kRedDark),
        primaryColorDark: const Color(kRed2)),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(kRedDark),
      primaryColor: const Color(kRed3),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(kRedDark),
      primaryColor: const Color(kRed4),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KPurpleDark),
      primaryColor: const Color(KPurple1),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KPurpleDark),
      primaryColor: const Color(KPurple2),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KPurpleDark),
      primaryColor: const Color(KPurple3),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KPurpleDark),
      primaryColor: const Color(KPurple4),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KBlueDark),
      primaryColor: const Color(KBlue1),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KBlueDark),
      primaryColor: const Color(KBlue2),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KBlueDark),
      primaryColor: const Color(KBlue3),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KBlueDark),
      primaryColor: const Color(KBlue4),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KGrayDark),
      primaryColor: const Color(KGray1),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KGrayDark),
      primaryColor: const Color(KGray2),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KGrayDark),
      primaryColor: const Color(KGray3),
    ),
    ThemeData.light().copyWith(
      backgroundColor: Colors.grey[200],
      primaryColorDark: const Color(KGrayDark),
      primaryColor: const Color(KGray4),
    ),
  ];

  static void changeThemeColor(index) async {
    final storage = const FlutterSecureStorage();

    await storage.write(key: "themeIndex", value: index.toString());
  }
}
